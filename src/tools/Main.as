package tools {
	import kiwi.debug.Log;

	import tools.utils.ASWingMessageAppender;

	import org.aswing.AsWingManager;

	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;

	/**
	 * @author zhangming.luo
	 */
	public class Main extends Sprite {
		private var context : AppContext;

		public function Main() {
			stage.align = StageAlign.TOP_LEFT;
			stage.frameRate = 60;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			AsWingManager.initAsStandard(this);
			Log.registerAlertAppender(new ASWingMessageAppender());
			context = new AppContext(this);
		}
	}
}
