package tools.body.view {
	import kiwi.util.MysqlHelper;
	import kiwi.util.reflect.ReflectHelper;

	import tools.model.BodyConfig;
	import tools.model.DatabaseConfig;

	import org.aswing.event.AWEvent;
	import org.osflash.signals.Signal;
	import org.robotlegs.mvcs.Mediator;

	/**
	 * 身体编辑器的中介
	 * @author zhangming.luo
	 */
	public class BodyEditorMediator extends Mediator {
		[Inject]
		public var view : BodyEditorPanel;
		[Inject]
		public var config : DatabaseConfig;
		private var _dataInit : Boolean = false;

		override public function onRegister() : void {
			super.onRegister();
			view.addEventListener(AWEvent.SHOWN, onShow);
		}

		private function onShow(event : AWEvent) : void {
			if (_dataInit) {
				return;
			}
			_dataInit = true;
			var signal : Signal = MysqlHelper.loadFromMysql(config.dbServer, config.dbPort, config.dbUsername, config.dbPassword, config.dbName, "front_body");
			signal.addOnce(function(array : Array) : void {
				config.bodyList = [];
				for each (var info:* in array) {
					var body : BodyConfig = new BodyConfig();
					ReflectHelper.duckAssign(info, body);
					body.postConstruct();
					config.bodyList.push(body);
				}
				view.setBodyList(config.bodyList);
			});
		}
	}
}
