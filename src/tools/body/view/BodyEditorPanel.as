package tools.body.view {
	import engine.animation.AnimationClip;

	import kiwi.event.ActionEvent;
	import kiwi.util.Tick;

	import tools.action.controller.LoadTextureAction;
	import tools.model.BodyConfig;
	import tools.texture.view.PreviewContainer;

	import org.aswing.JCheckBox;
	import org.aswing.JComboBox;
	import org.aswing.JList;
	import org.aswing.JPanel;
	import org.aswing.SoftBoxLayout;
	import org.aswing.border.TitledBorder;
	import org.aswing.event.InteractiveEvent;
	import org.aswing.event.ListItemEvent;

	/**
	 * 角色编辑
	 * @author zhangming.luo
	 */
	public class BodyEditorPanel extends JPanel {
		private var bodyList : JList;
		private var previewContainer : PreviewContainer;
		private var _body : BodyConfig;
		private var clip : AnimationClip;
		private var factory : AnimationFactory;
		private var stateCombo : JComboBox;
		private var directCombo : JComboBox;
		private var horseCheckBox : JCheckBox;
		private var bombCombo : JComboBox;
		private var _horse : Boolean;
		private var last : int;
		private var _state : String;

		public function BodyEditorPanel() {
			name = "角色编辑器";
			setLayout(new SoftBoxLayout(SoftBoxLayout.X_AXIS));
			initList();
			initPreview();
			setupEvent();
		}

		private function setupEvent() : void {
			bodyList.addEventListener(ListItemEvent.ITEM_CLICK, onSelectedBody);
			stateCombo.addEventListener(InteractiveEvent.SELECTION_CHANGED, onSelectedState);
			directCombo.addEventListener(InteractiveEvent.SELECTION_CHANGED, onSelectedDirect);
			horseCheckBox.addEventListener(InteractiveEvent.SELECTION_CHANGED, onSelectedHorse);
		}

		private function onSelectedHorse(event : InteractiveEvent) : void {
			_horse = horseCheckBox.isSelected();
			setBody(_body, true);
		}

		private function onSelectedDirect(event : InteractiveEvent) : void {
			setDirect(directCombo.getSelectedItem());
		}

		private function onSelectedState(event : InteractiveEvent) : void {
			setState(stateCombo.getSelectedItem());
		}

		private function onSelectedBody(event : ListItemEvent) : void {
			setBody(event.getValue());
		}

		private function initPreview() : void {
			previewContainer = new PreviewContainer();
			clip = new AnimationClip();
			Tick.getInstance().tickSignal.add(onFrame, "预览");
			previewContainer.setPreferredWidth(512);
			previewContainer.setPreferredHeight(512);
			append(previewContainer);
		}

		private function onFrame(delta : int) : void {
			last += delta;
			if (last < 1000 / 15) {
				return;
			}
			last = 0;
			if (factory) {
				clip.onFrame();
			}
		}

		private function initList() : void {
			var panel : JPanel = new JPanel();
			panel.setLayout(new SoftBoxLayout(SoftBoxLayout.Y_AXIS));
			bodyList = new JList();
			bodyList.setPreferredWidth(200);
			bodyList.setPreferredHeight(200);
			bodyList.setBorder(new TitledBorder(null, "基本角色"));
			panel.append(bodyList);
			stateCombo = new JComboBox();
			stateCombo.setBorder(new TitledBorder(null, "状态"));
			panel.append(stateCombo);
			directCombo = new JComboBox();
			directCombo.setBorder(new TitledBorder(null, "方向"));
			panel.append(directCombo);
			horseCheckBox = new JCheckBox("坐骑");
			panel.append(horseCheckBox);
			bombCombo = new JComboBox();
			bombCombo.setBorder(new TitledBorder(null, "爆炸效果"));
			panel.append(bombCombo);
			append(panel);
		}

		public function setBodyList(value : Array) : void {
			bodyList.setListData(value);
		}

		public function setBody(body : BodyConfig, force : Boolean = false) : void {
			if (_body == body && !force) {
				return;
			}
			stateCombo.setListData(body.getAllState(_horse));
			directCombo.setListData(body.getAllDirect());
			setState(body.getAllState(_horse)[0]);
			setDirect(body.getAllDirect()[0]);
			_body = body;
			factory = null;
			clip.unload();
			var action : LoadTextureAction = new LoadTextureAction(_horse ? body.exHorseUrl : body.url);
			action.addEventListener(ActionEvent.ACTION_COMPLETE, function() : void {
				// 显示动画
				factory = new AnimationFactory();
				factory.dic = action.animationDic;
				clip.unload();
				clip.registerAnimationFactory(factory);
				clip.state = _state;
				previewContainer.setDisplayObject(clip);
			});
			action.execute();
		}

		public function setState(state : String) : void {
			_state = state;
			clip.state = state;
			stateCombo.setSelectedItem(state);
		}

		public function setDirect(direct : String) : void {
			directCombo.setSelectedItem(direct);
		}
	}
}
import engine.animation.AnimationAction;
import engine.animation.IAnimationFactory;

import flash.utils.Dictionary;

class AnimationFactory implements IAnimationFactory {
	public var dic : Dictionary ;

	public function getAnimation(name : String) : AnimationAction {
		return dic[name];
	}

	public function unload() : void {
	}
}
