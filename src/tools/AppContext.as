package tools {
	import flash.display.DisplayObjectContainer;

	import org.robotlegs.base.ContextEvent;
	import org.robotlegs.mvcs.Context;

	import tools.global.controller.StartUpCommand;

	/**
	 * @author zhangming.luo
	 */
	public class AppContext extends Context {
		public function AppContext(contextView : DisplayObjectContainer = null, autoStartup : Boolean = true) {
			super(contextView, autoStartup);
		}

		override public function startup() : void {
			// 注册起动的command
			commandMap.mapEvent(ContextEvent.STARTUP, StartUpCommand, ContextEvent, true);
			// 发送事件，触发起动的command
			dispatchEvent(new ContextEvent(ContextEvent.STARTUP));
		}
	}
}
