package tools.project.controller.action {
	import ghostcat.fileformat.png.PNGEncoder;

	import kiwi.action.Action;

	import flash.display.BitmapData;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	/**
	 * @author zhangming.luo
	 */
	public class SavePNGAction extends Action {
		private var filename : String;
		private var bitmapData : BitmapData;

		public function SavePNGAction(filename : String, bitmapData : BitmapData) {
			this.bitmapData = bitmapData;
			this.filename = filename;
		}

		override public function execute() : void {
			super.execute();
			var file : FileStream = new FileStream();
			file.open(new File(filename), FileMode.WRITE);
			file.writeBytes(PNGEncoder.encode(bitmapData));
			file.close();
			result();
		}
	}
}
