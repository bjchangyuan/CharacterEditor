package tools.project.controller.action {
	import kiwi.action.Action;

	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	/**
	 * 保存xml文件
	 * @author zhangming.luo
	 */
	public class SaveTextAction extends Action {
		private var filename : String;
		private var text : String;

		public function SaveTextAction(filename : String, text : String) {
			this.text = text;
			this.filename = filename;
		}

		override public function execute() : void {
			super.execute();
			var file : FileStream = new FileStream();
			file.open(new File(filename), FileMode.WRITE);
			file.writeUTFBytes(text);
			file.close();
			result();
		}
	}
}
