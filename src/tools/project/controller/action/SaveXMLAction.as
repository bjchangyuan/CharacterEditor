package tools.project.controller.action {
	import kiwi.action.Action;

	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;

	/**
	 * 保存xml文件
	 * @author zhangming.luo
	 */
	public class SaveXMLAction extends Action {
		private var filename : String;
		private var xml : XML;

		public function SaveXMLAction(filename : String, xml : XML) {
			this.xml = xml;
			this.filename = filename;
		}

		override public function execute() : void {
			super.execute();
			var file : FileStream = new FileStream();
			file.open(new File(filename), FileMode.WRITE);
			file.writeUTFBytes(xml.toString());
			file.close();
			result();
		}
	}
}
