package tools.project.controller.action {
	import kiwi.action.Action;
	import kiwi.util.sprintf;

	import tools.model.TileInfo;

	/**
	 * @author zhangming.luo
	 */
	public class SaveTilesAction extends Action {
		private var dir : String;
		private var allTile : Array;

		public function SaveTilesAction(dir : String, allTile : Array) {
			this.allTile = allTile;
			this.dir = dir;
		}

		override public function execute() : void {
			super.execute();
			for each (var tile:TileInfo in allTile) {
				new SavePNGAction(sprintf("%s/%s.png", dir, tile.id), tile.getBitmapData()).execute();
			}
			result();
		}
	}
}
