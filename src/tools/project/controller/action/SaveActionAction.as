package tools.project.controller.action {
	import kiwi.action.Action;

	/**
	 * @author zhangming.luo
	 */
	public class SaveActionAction extends Action {
		private var dir : String;
		private var allAction : Array;

		public function SaveActionAction(dir : String, allAction : Array) {
			this.allAction = allAction;
			this.dir = dir;
		}

		override public function execute() : void {
			super.execute();
			result();
		}
	}
}
