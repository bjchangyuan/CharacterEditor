package tools.project.controller.action {
	import kiwi.action.Action;
	import kiwi.util.sprintf;

	import tools.model.FrameInfo;

	/**
	 * @author zhangming.luo
	 */
	public class SaveFramesAction extends Action {
		private var dir : String;
		private var allFrames : Array;

		public function SaveFramesAction(dir : String, allFrames : Array) {
			this.allFrames = allFrames;
			this.dir = dir;
		}

		override public function execute() : void {
			super.execute();
			for each (var frame:FrameInfo in allFrames) {
				new SavePNGAction(sprintf("%s/%s.png", dir, frame.id), frame.getBitmapData()).execute();
			}
			result();
		}
	}
}
