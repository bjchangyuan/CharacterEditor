package tools.project.controller {
	import kiwi.data.json.decodeJson;

	import tools.event.ProjectEvent;
	import tools.model.EditorConfig;
	import tools.model.TileInfo;

	import org.robotlegs.mvcs.Command;

	import flash.geom.Point;
	import flash.geom.Rectangle;

	/**
	 * 读取素材配置
	 * @author zhangming.luo
	 */
	public class OpenTextureConfCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		[Inject]
		public var event : ProjectEvent;

		override public function execute() : void {
			var textureConfig : * = decodeJson(String(event.file.data));
			var tiles : Array = [];
			var frames : Array = [];
			var index : int = 0;
			for each (var frame : * in textureConfig.frames) {
				var tileInfo : TileInfo = new TileInfo();
				tileInfo.$source = config.texture;
				tileInfo.name = frame.filename;
				tileInfo.id = ++index;
				tileInfo.source = new Rectangle(frame.frame.x, frame.frame.y, frame.frame.w, frame.frame.h);
				tileInfo.registration = new Point(256 - frame.spriteSourceSize.x, 256 - frame.spriteSourceSize.y);
				if (tileInfo.source.width == 0 || tileInfo.source.height == 0) {
					continue;
				}
				tiles.push(tileInfo);
			}
			config.allFrameList = frames;
			config.allTileList = tiles;
		}
	}
}
