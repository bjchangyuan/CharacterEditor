package tools.project.controller {
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import ghostcat.fileformat.png.PNGEncoder;
	
	import kiwi.debug.Log;
	import kiwi.util.ArrayUtils;
	import kiwi.util.sprintf;
	
	import org.robotlegs.mvcs.Command;
	
	import tools.event.ProjectEvent;
	import tools.model.ActionInfo;
	import tools.model.EditorConfig;
	import tools.model.FrameInfo;
	import tools.model.TileInfo;

	/**
	 * 保存(解)工作进度到XML文件
	 * @author zhangming.luo
	 */
	public class SaveTempProjectCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		[Inject]
		public var event : ProjectEvent;

		override public function execute() : void {
			super.execute();
			
//			for each(var action:ActionInfo in config.actionList){
//				
//			}
			
			var xml : XML = <config>
			<texture></texture>
			<tiles></tiles>
			<frames></frames>
			<actions></actions>
			</config>;
			XML(xml.texture).appendChild(event.file.name.toString().replace(".sprite",".png"));
			for each (var tile:TileInfo in config.allTileList) {
				XML(xml.tiles).appendChild(createXMLByObject("item", {id:tile.id, name:tile.name, source:{x:tile.source.x, y:tile.source.y, width:tile.source.width, height:tile.source.height}, registration:{x:tile.registration.x, y:tile.registration.y}}));
			}
			for each (var frame:FrameInfo in config.allFrameList) {
				XML(xml.frames).appendChild(createXMLByObject("item", {id:frame.id, name:frame.name, tileList:ArrayUtils.collect(frame.tileList, "id")}));
			}
			for each (var action:ActionInfo in config.actionList) {
				XML(xml.actions).appendChild(createXMLByObject("action", {scale:action.scale, name:action.name, frame_duration:ArrayUtils.collect(action.frameList, "duration"), frame:ArrayUtils.collect(ArrayUtils.collect(action.frameList, "frame"), "id")}));
			}
			if (event.file) {
				var out : FileStream = new FileStream();
				out.open(event.file, FileMode.WRITE);
				out.writeUTFBytes(xml.toString());
				out.close();
				onSaved();
			} else {
				var file : File = new File();
				file.addEventListener(Event.COMPLETE, function() : void {
					event.file = file;
					onSaved();
				});
				file.save(xml.toString(), event.file.name);
			}
		}

		private function onSaved() : void {
			// 重新保存一个texture到sprite所在的目录
			var texture : File = event.file.parent.resolvePath("texture.png");
			if (!texture.exists) {
				var out : FileStream = new FileStream();
				out.open(texture, FileMode.WRITE);
				out.writeBytes(PNGEncoder.encode(config.texture.bitmapData));
				out.close();
			}
			config.projectDir = event.file.parent;
			config.projectName = event.file.name.toLowerCase().replace(".sprite", "");
			event.ignorInfo || Log.alert("保存完毕");
		}

		private function createXMLByObject(title : String, obj : *) : XML {
			if (obj is int || obj is Number || obj is String || obj is Boolean || obj is uint) {
				return new XML(sprintf("<%s>%s</%s>", title, obj, title));
			}
			var xml : XML = new XML(sprintf("<%s></%s>", title, title));
			for (var key:String in obj) {
				var subXML : XML;
				if (obj[key] is Array) {
					subXML = new XML(sprintf("<%s></%s>", key, key));
					for each (var subObj:* in obj[key]) {
						subXML.appendChild(createXMLByObject("item", subObj));
					}
				} else {
					subXML = createXMLByObject(key, obj[key]);
				}
				xml.appendChild(subXML);
			}
			return xml;
		}
	}
}
