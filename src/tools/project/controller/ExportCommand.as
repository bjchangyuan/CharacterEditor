package tools.project.controller {
	import ghostcat.operation.FunctionOper;
	import ghostcat.operation.Queue;

	import kiwi.debug.Log;

	import tools.model.EditorConfig;

	import org.aswing.JOptionPane;
	import org.robotlegs.mvcs.Command;

	import flash.filesystem.File;

	/**
	 * 批量导出
	 * @author zhangming.luo
	 */
	public class ExportCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		private var baseDir : String;

		override public function execute() : void {
			JOptionPane.showInputDialog("选择目录", "输入需要批量转换的目录", handleInput, "C:\\Users\\Mage\\Desktop\\static\\asset");
		}

		private function handleInput(path : String) : void {
			var dir : File = File.applicationDirectory.resolvePath(path);
			baseDir = dir.nativePath;
			if (!dir.exists) {
				Log.error("目录不存在");
				return;
			}
			ExportOper.config = config;
			ExportOper.baseDir = baseDir;
			var tmp : Array = [dir];
			var operList : Array = [];
			while (tmp.length) {
				for each (var file:File in tmp.pop().getDirectoryListing()) {
					if (file.isDirectory) {
						tmp.push(file);
						continue;
					}
					if (file.extension == "swf") {
						operList.push(new ExportOper(file, 1));
					} else if (file.extension == "ani") {
						operList.push(new ExportOper(file, 2));
					}
				}
			}
			operList.sort(function(a : ExportOper, b : ExportOper) : Number {
				return a.file.size == b.file.size ? 0 : (a.file.size > b.file.size ? 1 : -1);
			});
			operList.push(new FunctionOper(Log.alert, ["结束了"]));
			var queue : Queue = new Queue(operList);
			queue.execute();
		}
	}
}
import engine.animation.AnimationFrame;
import engine.animation.AnimationAction;
import engine.animation.Frame;
import engine.animation.Texture2AnimationDictionaryAction;
import engine.datatype.AssetOrderType;

import ghostcat.fileformat.png.PNGEncoder;
import ghostcat.operation.Oper;

import kiwi.event.ActionEvent;

import tools.model.EditorConfig;

import flash.display.BitmapData;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.geom.Point;
import flash.utils.Dictionary;

class ExportOper extends Oper {
	public static var config : EditorConfig;
	public static var baseDir : String;
	public var file : File;
	private var version : int;
	private var dic : Dictionary;
	private var base : File;

	public function ExportOper(file : File, version : int) {
		this.version = version;
		this.file = file;
	}

	override public function execute() : void {
		super.execute();
		var action : Texture2AnimationDictionaryAction = new
		Texture2AnimationDictionaryAction(file.nativePath.replace(/\\/g, "/"), AssetOrderType.ORDER_PRE);
		action.addEventListener(ActionEvent.ACTION_COMPLETE, handleComplete);
		action.execute();
		dic = action.animationDic;
	}

	private function handleComplete(item : *) : void {
		base = file.parent.resolvePath(file.name.replace("." + file.extension, ""));
		base.createDirectory();
		for (var key:String in dic) {
			base.resolvePath(key).createDirectory();
			var animationAction : AnimationAction = dic[key];
			var cnt : int = 1;
			for each (var frame:AnimationFrame in animationAction.frames) {
				var w : int = 512,c : int = 256;
				if (frame.frame.$width > 512 || frame.frame.$height > 512) {
					w = 1024;
					c = 512;
				}
				var bd : BitmapData = new BitmapData(w, w, true, 0x000000);
				frame.frame.bitmapDraw(bd, new Point(c, c));
				saveBitmap(bd, base.resolvePath(key + "/" + cnt++ + ".png"));
			}
		}
		result();
	}

	private function saveBitmap(bd : BitmapData, path : File) : void {
		var out : FileStream = new FileStream();
		out.open(path, FileMode.WRITE);
		out.writeBytes(PNGEncoder.encode(bd));
		out.close();
	}
}
