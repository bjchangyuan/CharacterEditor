package tools.project.controller
{
	import flash.filesystem.File;
	
	import ghostcat.operation.FunctionOper;
	import ghostcat.operation.Queue;
	
	import kiwi.debug.Log;
	
	import org.aswing.JOptionPane;
	import org.robotlegs.mvcs.Command;
	
	public class AniTestCommand extends Command
	{
		public function AniTestCommand()
		{
			super();
		}
		
		private var baseDir : String;
		
		override public function execute() : void {
			JOptionPane.showInputDialog("选择目录", "输入需要测试的ANI的目录", handleInput, "e:\\Desktop\\body");
		}
		
		private function handleInput(path : String) : void {
			var dir : File = File.applicationDirectory.resolvePath(path);
			baseDir = dir.nativePath;
			if (!dir.exists) {
				Log.error("目录不存在");
				return;
			}
			
			var operList : Array = [];
			
			for each(var file:File in dir.getDirectoryListing()){
				if(file.extension == "ani"){
					operList.push(new TransOper(file));
				}
			}
			
			operList.sort(function(a : TransOper, b : TransOper) : Number {
				return a.file.size == b.file.size ? 0 : (a.file.size > b.file.size ? 1 : -1);
			});
			operList.push(new FunctionOper(Log.alert, ["结束了"]));
			var queue : Queue = new Queue(operList);
			queue.execute();
			
		}
	}
}
import com.greensock.loading.SWFLoader;
import com.greensock.loading.data.SWFLoaderVars;

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;

import ghostcat.fileformat.png.PNGEncoder;
import ghostcat.operation.Oper;

import kiwi.debug.Log;

import tools.model.EditorConfig;


class TransOper extends Oper {
	public static var config : EditorConfig;
	public static var baseDir : String;
	public var file : File;
	private var loader : SWFLoader;
	private var dir : File;
	private var interval : uint;
	
	public function TransOper(file : File) {
		this.file = file;
		dir = file.parent/*.resolvePath(file.name.replace("." + file.extension, ""))*/;
	}
	
	override public function execute() : void {
		super.execute();
		loader = new SWFLoader(file.nativePath, new SWFLoaderVars().onComplete(handleComplete));
		loader.load();
	}
	
	private function handleComplete(item : *) : void {
		dir.createDirectory();
		var textureClz : Class = loader.getClass("TEXTURE_"+file.parent.name+"_"+file.name.replace(".ani",""));
		var textureConfClz : Class = loader.getClass("TEXTURE_CONF_"+file.parent.name+"_"+file.name.replace(".ani",""));
		if(!textureClz){
			textureClz = loader.getClass("TEXTURE_"+file.name.replace(".ani",""));
			textureConfClz = loader.getClass("TEXTURE_CONF_"+file.name.replace(".ani",""));
		}
		var source : * = new textureClz;
		var bd : BitmapData = source is Bitmap ? source.bitmapData : source;
		var xml : XML = new XML(new textureConfClz());
		saveBitmap(bd, dir.resolvePath("./"+file.name.replace(file.type,"")+".png"));
		saveXML(xml, dir.resolvePath("./"+file.name.replace(file.type,"")+".xml"));
		Log.info("导出完毕 " + dir.nativePath);
		loader.unload();
	}
	
	private function saveXML(xml : XML, path : File) : void {
		var out : FileStream = new FileStream();
		out.open(path, FileMode.WRITE);
		out.writeUTFBytes(xml.toXMLString());
		out.close();
	}
	
	private function saveBitmap(bd : BitmapData, path : File) : void {
		var out : FileStream = new FileStream();
		out.open(path, FileMode.WRITE);
		out.writeBytes(PNGEncoder.encode(bd));
		out.close();
	}

}