package tools.project.controller
{
	import flash.desktop.NativeProcess;
	import flash.desktop.NativeProcessStartupInfo;
	import flash.events.Event;
	import flash.events.NativeProcessExitEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	
	import ghostcat.operation.Queue;
	
	import kiwi.debug.Log;
	
	import org.aswing.JOptionPane;
	import org.robotlegs.mvcs.Command;
	
	import tools.event.ProjectEvent;
	import tools.model.EditorConfig;
	
	public class Jsfl2SwfCommand extends Command
	{
		[Inject]
		public var config : EditorConfig;
		[Inject]
		public var event : ProjectEvent;
		private var rootDir : File;
		private var root : String;
		private var queueList:Array;
		
		private var continueTime:int = 5;
		
		override public function execute() : void {
			super.execute();
			if (event.file) {
				rootDir = event.file;
				onSelected();
			} else {
				JOptionPane.showInputDialog("提示", "打开JSFL的所在目录", function(path : String) : void {
					if (!path) {
						return;
					}
					rootDir = File.applicationDirectory.resolvePath(path);
					if (!rootDir.exists) {
						Log.alert("目录不存在");
					} else if (!rootDir.isDirectory) {
						Log.alert("请选择文件夹");
					} else {
						onSelected();
					}
				},"e:\\Desktop\\mengjiang");
			}
		}
		
		private function onSelected(event : Event = null) : void {
			if (config.flash == null) {
				Log.alert("Flash没有找到");
				return;
			}
			var flash : File = File.applicationDirectory.resolvePath(config.flash);
			if (!flash.exists) {
				Log.alert("Flash.exe没有找到");
				return;
			}
			
			root = rootDir.nativePath;
			var files : Array = rootDir.getDirectoryListing();
			queueList = new Array();
			for each (var file:File in files) {
				if(!file.isDirectory&&file.type == ".jsfl"){
//					run(flash,[file.nativePath]);
					queueList.push(file.nativePath);
				}
			}
//			var interval:int = setInterval(function():void{
//				if(queueList.length == 0 || !queueList){
//					clearInterval(interval);
//					Log.alert("JSFL2SWF搞完了");
//				}else if(File.applicationDirectory.resolvePath(queueList[queueList.length-1]).exists){
//					run(flash,[queueList.pop()]);
//				}
//			},1500);
			Log.alert("等待批处理中...大概:"+queueList.length*continueTime+"秒后exit");
			packRun(flash,queueList);
		}
		
		private function packRun(flash:File,arr:Array):void{
			run(flash,[arr.pop()],function():void{
				trace("exitHandle");
				if(arr.length == 0 || !arr){
					Log.alert("JSFL2SWF搞完了");
					return;
				}else{
					trace("exitHandle-->packRun");
					setTimeout(function():void{
						packRun(flash,arr);
					},continueTime*1000);
				}
			});
		}
		
		private function run(file : File, arg : Array = null, exitHandler : Function = null, traceHandler : Function = null, errorHandler : Function = null, workingDirectory : File = null) : void {
			var nativeProcessStartupInfo : NativeProcessStartupInfo = new NativeProcessStartupInfo();
			nativeProcessStartupInfo.executable = file;
			nativeProcessStartupInfo.workingDirectory = workingDirectory;
			if (arg) {
				var vector : Vector.<String> = new Vector.<String>();
				vector.push.apply(null, arg);
				nativeProcessStartupInfo.arguments = vector;
			}
			var process : NativeProcess = new NativeProcess();
			if (dataHandler != null) {
				process.addEventListener(ProgressEvent.STANDARD_OUTPUT_DATA, dataHandler);
				process.addEventListener(ProgressEvent.STANDARD_ERROR_DATA, dataHandler);
			}
			if (exitHandler != null) {
				process.addEventListener(NativeProcessExitEvent.EXIT, exitHandler);
			}
			
			process.start(nativeProcessStartupInfo);
			
			return;
			function dataHandler(event : Event) : void {
				var process : NativeProcess = event.currentTarget as NativeProcess;
				var str : String;
				if (event.type == ProgressEvent.STANDARD_OUTPUT_DATA) {
					str = process.standardOutput.readMultiByte(process.standardOutput.bytesAvailable, "gb2312").toString();
					if (traceHandler != null) {
						traceHandler(str);
					}
				} else {
					str = process.standardError.readMultiByte(process.standardError.bytesAvailable, "gb2312").toString();
					if (errorHandler != null) {
						errorHandler(str);
					}
				}
			}
		}
		
	}
}