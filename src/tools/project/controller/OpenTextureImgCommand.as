package tools.project.controller {
	import tools.event.ProjectEvent;
	import tools.model.EditorConfig;

	import org.robotlegs.mvcs.Command;

	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;

	/**
	 * 读取一个素材文件
	 * @author zhangming.luo
	 */
	public class OpenTextureImgCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		[Inject]
		public var event : ProjectEvent;

		override public function execute() : void {
			var loader : Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function() : void {
				config.texture.file = event.file.nativePath;
				config.texture.bitmapData = Bitmap(loader.contentLoaderInfo.content).bitmapData.clone();
				config.allTileList = [];
				config.allFrameList = [];
			});
			loader.loadBytes(event.file.data);
			dispatch(new ProjectEvent(ProjectEvent.TEXTURE_IMG_LOADED));
		}
	}
}
