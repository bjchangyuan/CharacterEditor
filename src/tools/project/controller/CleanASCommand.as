package tools.project.controller
{
	import flash.events.Event;
	import flash.filesystem.File;
	
	import kiwi.debug.Log;
	
	import org.aswing.JOptionPane;
	import org.robotlegs.mvcs.Command;
	
	import tools.event.ProjectEvent;

	public class CleanASCommand extends Command
	{
		public function CleanASCommand()
		{
			super();
		}
		
		[Inject]
		public var event : ProjectEvent;
		private var rootDir : File;
		
		override public function execute() : void {
			super.execute();
			
			if (event.file) {
				rootDir = event.file;
				onSelected();
			} else {
				JOptionPane.showInputDialog("提示", "打开AS的所在目录", function(path : String) : void {
					if (!path) {
						return;
					}
					rootDir = File.applicationDirectory.resolvePath(path);
					if (!rootDir.exists) {
						Log.alert("目录不存在");
					} else if (!rootDir.isDirectory) {
						Log.alert("请选择文件夹");
					} else {
						onSelected();
					}
				},"e:\\Desktop\\body");
			}
		}
		
		private function onSelected(event : Event = null) : void {
			pachRun(rootDir);
		}
		
		private function pachRun(file:File):void{
			for each(var childFile:File in file.getDirectoryListing()){
				if(childFile.isDirectory){
					pachRun(childFile);
				}else{
					if(childFile.type == ".as"){
						trace("移除文件:"+childFile.nativePath);
						childFile.deleteFile();
					}
				}
			}
		}
	}
}