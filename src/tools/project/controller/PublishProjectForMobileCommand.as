package tools.project.controller {
	import kiwi.action.queue.Queue;
	import kiwi.data.json.encodeJson;
	import kiwi.debug.Log;
	import kiwi.event.ActionEvent;
	import kiwi.util.ArrayUtils;

	import tools.event.ProjectEvent;
	import tools.model.ActionInfo;
	import tools.model.EditorConfig;
	import tools.model.FrameInfo;
	import tools.model.TileInfo;
	import tools.project.controller.action.SavePNGAction;
	import tools.project.controller.action.SaveTextAction;

	import org.robotlegs.mvcs.Command;

	import flash.events.Event;
	import flash.filesystem.File;

	/**
	 * @author zhangming.luo
	 */
	public class PublishProjectForMobileCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		[Inject]
		public var event : ProjectEvent;
		private var dir : File;
		private var queue : Queue;

		override public function execute() : void {
			dispatch(new ProjectEvent(ProjectEvent.SAVE, false, false, config.currentName, true));
			if (config.projectDir) {
				dir = config.projectDir;
				startPublish();
			} else {
				dir = File.desktopDirectory;
				dir.addEventListener(Event.SELECT, function() : void {
					startPublish();
				});
				dir.browseForDirectory("发布到");
			}
		}

		private function startPublish() : void {
			queue = new Queue(null);
			// 保存素材图片
			queue.submit(new SavePNGAction(dir.nativePath + "/" + config.projectName + ".png", config.texture.bitmapData));
			// 保存sprite的配置
			queue.submit(new SaveTextAction(dir.nativePath + "/" + config.projectName + ".json", genericConfigJSON()));
			queue.addEventListener(ActionEvent.ACTION_COMPLETE, onComplete);
			queue.execute();
		}

		private function onComplete(e : Event) : void {
			if (!this.event.ignorInfo) {
				Log.alert("发布完毕");
			}
			dispatch(new ProjectEvent(ProjectEvent.PUBLISH_COMPLETE));
		}

		/**
		 * config={
		 *  name:"test",
		 * 	tiles:[
		 * 	{
		 * 		"id":1,
		 * 		"source":{
		 * 			"x":0,
		 * 			"y":0,
		 * 			"width":100,
		 * 			"height":100
		 * 		}
		 * 		"registration":{
		 * 			"x":50,
		 * 			"y":50
		 * 		}
		 * 	}
		 * 	],
		 * 	frames:[
		 * 	{
		 * 		"id":1,
		 * 		"tile":[1]
		 * 	}
		 * 	],
		 * 	actions:[
		 * 	{
		 * 		"name":"test",
		 * 		"frame":[1,2,3,4,5]
		 * 	}
		 * 	],
		 * }
		 */
		private function genericConfigJSON() : String {
			var result : Object = {};
			// tiles部分
			var tiles : Array = [];
			for each (var tile:TileInfo in config.allTileList) {
				tiles.push({id:tile.id, source:{x:tile.source.x, y:tile.source.y, width:tile.source.width, height:tile.source.height}, registration:{x:tile.registration.x, y:tile.registration.y}});
			}
			// frame部分
			var frames : Array = [];
			for each (var frame:FrameInfo in config.allFrameList) {
				frames.push({id:frame.id, tile:ArrayUtils.collect(frame.tileList, "id")});
			}
			// action部分
			var actions : Array = [];
			for each (var action:ActionInfo in config.actionList) {
				actions.push({scale:action.scale, name:action.name, frame_duration:ArrayUtils.collect(action.frameList, "duration"), frame:ArrayUtils.collect(ArrayUtils.collect(action.frameList, "frame"), "id")});
			}
			result.name = config.projectName;
			result.tiles = tiles;
			result.frames = frames;
			result.actions = actions;
			return encodeJson(result);
		}
	}
}
