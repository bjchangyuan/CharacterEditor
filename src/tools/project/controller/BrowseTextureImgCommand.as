package tools.project.controller {
	import tools.event.ProjectEvent;
	import tools.utils.FileHelper;

	import org.robotlegs.mvcs.Command;

	import flash.filesystem.File;
	import flash.utils.ByteArray;

	/**
	 * 浏览并打开素材图片
	 * @author zhangming.luo
	 */
	public class BrowseTextureImgCommand extends Command {
		private var file : File;

		override public function execute() : void {
			file = FileHelper.loadFile(fileLoaded, FileHelper.filters("png"));
		}

		private function fileLoaded(bytes : ByteArray) : void {
			var event : ProjectEvent = new ProjectEvent(ProjectEvent.OPEN_TEXTURE_IMG);
			event.file = file;
			dispatch(event);
		}
	}
}
