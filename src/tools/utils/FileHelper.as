package tools.utils {
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.net.FileFilter;

	/**
	 * @author zhangming.luo
	 */
	public class FileHelper {
		public static function loadFile(callback : Function, fileFilters : Array = null) : File {
			var file : File = new File();
			file.addEventListener(Event.SELECT, function(e : Event) : void {
				file.addEventListener(Event.COMPLETE, function(e : Event) : void {
					callback(file.data);
				}, false, 0, true);
				file.load();
			}, false, 0, true);
			file.browseForOpen("打开文件", fileFilters);
			return file;
		}

		public static function filters(filter : String, ...filters) : Array {
			var result : Array = [new FileFilter("*." + filter, "*." + filter)];
			for each (filter in filters) {
				result.push(new FileFilter("*." + filter, "*." + filter));
			}
			return result;
		}
	}
}
