package tools.utils {
	import kiwi.debug.ILogAppender;
	import kiwi.util.sprintf;

	import org.aswing.JOptionPane;

	/**
	 * @author zhangming.luo
	 */
	public class ASWingMessageAppender implements ILogAppender {
		public function append(label : String, msg : String) : void {
			JOptionPane.showMessageDialog("警告", sprintf("%-20s", msg), null, null, true, null, JOptionPane.OK);
		}
	}
}
