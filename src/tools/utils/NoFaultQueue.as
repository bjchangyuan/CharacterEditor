package tools.utils {
	import kiwi.action.queue.Queue;
	/**
	 * 忽略所有错误的队列?
	 * @author zhangming.luo
	 */
	public class NoFaultQueue extends Queue {
		override public function fault() : void {
			super.result();
		}
	}
}
