package tools.utils {
	import kiwi.util.data.ChangeWatcher;

	import flash.events.IEventDispatcher;
	/**
	 * @author zhangming.luo
	 */
	public function bindSetter(setter : Function, sHost : IEventDispatcher, sProp : String, init : Boolean = false) : ChangeWatcher {
		if (init) {
			setter(sHost[sProp]);
		}
		var watcher : ChangeWatcher = new ChangeWatcher(sHost, sProp, setter);
		return (watcher);
	}
}
