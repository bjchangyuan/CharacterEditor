package tools.action.controller {
	import kiwi.debug.Log;

	import tools.event.ActionEditorEvent;
	import tools.model.EditorConfig;

	import com.adobe.utils.ArrayUtil;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author zhangming.luo
	 */
	public class ChangeFramePositionCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		[Inject]
		public var event : ActionEditorEvent;

		override public function execute() : void {
			super.execute();
			if (event.action == null) {
				Log.alert("还没有选择动作");
				return;
			}
			if (event.frame == null) {
				Log.alert("还没有选择对应的帧");
				return;
			}
			ArrayUtil.removeValueFromArray(event.action.frameList, event.frame);
			event.action.frameList.splice(event.pos, -1, event.frame);
			config.curAction = event.action;
		}
	}
}
