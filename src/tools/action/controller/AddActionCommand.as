package tools.action.controller {
	import tools.event.ActionEditorEvent;
	import tools.model.EditorConfig;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author zhangming.luo
	 */
	public class AddActionCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		[Inject]
		public var event : ActionEditorEvent;

		override public function execute() : void {
			var newList : Array = config.actionList.concat();
			newList.push(event.action);
			config.actionList = newList;
		}
	}
}
