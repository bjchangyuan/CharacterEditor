package tools.action.controller {
	import kiwi.debug.Log;

	import tools.event.ActionEditorEvent;
	import tools.model.EditorConfig;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author zhangming.luo
	 */
	public class AddFrameToActionCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		[Inject]
		public var event : ActionEditorEvent;

		override public function execute() : void {
			super.execute();
			if (event.action == null) {
				Log.alert("还没有选择动作");
				return;
			}
			if (event.frame == null) {
				Log.alert("还没有选择帧");
				return;
			}
			event.action.frameList.push(event.frame);
			config.curAction = event.action;
		}
	}
}

