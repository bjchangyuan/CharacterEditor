package tools.action.controller {
	import com.adobe.utils.ArrayUtil;

	import tools.event.ActionEditorEvent;
	import tools.model.EditorConfig;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author zhangming.luo
	 */
	public class RemoveActionCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		[Inject]
		public var event : ActionEditorEvent;

		override public function execute() : void {
			super.execute();
			var newList : Array = config.actionList.concat();
			ArrayUtil.removeValueFromArray(newList, event.action);
			config.actionList = newList;
		}
	}
}
