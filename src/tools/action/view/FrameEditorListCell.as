package tools.action.view {
	import tools.model.ActionFrameInfo;

	import org.aswing.ASColor;
	import org.aswing.Component;
	import org.aswing.Container;
	import org.aswing.JLabel;
	import org.aswing.JPanel;
	import org.aswing.JTextField;
	import org.aswing.SoftBoxLayout;
	import org.aswing.border.BevelBorder;
	import org.aswing.event.AWEvent;
	import org.aswing.ext.GridList;
	import org.aswing.ext.GridListCell;

	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.TextEvent;

	/**
	 * @author zhangming.luo
	 */
	public class FrameEditorListCell implements GridListCell {
		private var frameInfo : ActionFrameInfo;
		private var panel : JPanel;
		private var canvas : Bitmap;
		private var frameLabel : JLabel;
		private var frameDurationTextField : JTextField;
		private var gridList : GridList;

		public function setCellValue(value : *) : void {
			frameInfo = value as ActionFrameInfo;
			getPreview();
			if (frameInfo) {
				canvas.bitmapData = frameInfo.frame.getBitmapData();
				canvas.width = 40;
				canvas.scaleY = canvas.scaleX;
				frameLabel.setText(frameInfo.toString());
				frameDurationTextField.setText(frameInfo.duration.toString());
			}
		}

		public function getCellComponent() : Component {
			return getPreview();
		}

		public function getPreview() : Component {
			if (!panel) {
				panel = new JPanel();
				panel.setLayout(new SoftBoxLayout(SoftBoxLayout.X_AXIS));
				panel.setOpaque(true);
				panel.setFocusable(false);
				panel.setBorder(new BevelBorder());
				frameLabel = new JLabel("");
				frameLabel.setPreferredHeight(15);
				panel.append(frameLabel);
				var container : Container = new Container();
				canvas = new Bitmap();
				container.addChild(canvas);
				container.setPreferredWidth(50);
				container.setPreferredHeight(50);
				panel.append(container);
				panel.append(frameDurationTextField = new JTextField("1", 3));
				frameDurationTextField.addEventListener(AWEvent.ACT, onAct);
				frameDurationTextField.getTextField().addEventListener(Event.CHANGE, onAct);
			}
			return panel;
		}

		private function onAct(event : Event) : void {
			frameInfo.duration = int(frameDurationTextField.getText());
		}

		public function setGridListCellStatus(gridList : GridList, selected : Boolean, index : int) : void {
			this.gridList = gridList;
			var c : ASColor = index % 2 == 0 ? ASColor.WHITE : ASColor.GRAY;
			if (selected) {
				c = ASColor.BLUE;
			}
			panel.setBackground(c);
		}

		public function getCellValue() : * {
			return frameInfo;
		}
	}
}
