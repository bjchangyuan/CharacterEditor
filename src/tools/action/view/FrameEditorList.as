package tools.action.view {
	import org.aswing.ext.GeneralGridListCellFactory;
	import org.aswing.ext.GridList;

	/**
	 * 帧编辑列表
	 * @author zhangming.luo
	 */
	public class FrameEditorList extends GridList {
		public function FrameEditorList() {
			super(null, new GeneralGridListCellFactory(FrameEditorListCell));
			setTileWidth(250);
			setTileHeight(85);
		}

		override public function updateUI() : void {
			setColumns(1);
			if (model && model.getSize()) {
				setRows(model.getSize());
			} else {
				setRows(99);
			}
			super.updateUI();
		}
	}
}
