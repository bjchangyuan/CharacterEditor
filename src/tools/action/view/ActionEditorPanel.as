package tools.action.view {
	import tools.event.ActionEditorEvent;
	import tools.global.view.FrameList;
	import tools.model.ActionFrameInfo;
	import tools.model.ActionInfo;
	import tools.model.EmptyFrameInfo;
	import tools.model.FrameInfo;

	import org.aswing.Container;
	import org.aswing.DefaultListSelectionModel;
	import org.aswing.JButton;
	import org.aswing.JPanel;
	import org.aswing.JScrollPane;
	import org.aswing.JTable;
	import org.aswing.SoftBoxLayout;
	import org.aswing.VectorListModel;
	import org.aswing.border.BevelBorder;
	import org.aswing.border.TitledBorder;
	import org.aswing.event.SelectionEvent;
	import org.aswing.ext.GridListItemEvent;
	import org.aswing.table.PropertyTableModel;
	import org.aswing.table.sorter.TableSorter;

	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author zhangming.luo
	 */
	public class ActionEditorPanel extends JPanel {
		public var allFrameList : FrameList;
		public var actionTable : JTable;
		public var actionListModel : VectorListModel;
		public var actionFrameList : FrameEditorList;
		private var addActionButton : JButton;
		private var delActionButton : JButton;
		private var copyActionButton : JButton;
		private var removeAllActionButton : JButton;
		private var addFrameToButton : JButton;
		private var delFrameFromButton : JButton;
		private var removeAllFrameFromButton : JButton;
		private var moveUpFrameButton : JButton;
		private var moveDownFrameButton : JButton;
		private var actionPreview : ActionPreview;
		private var addEmptyFrameToButton : JButton;

		public function ActionEditorPanel() {
			name = "动作编辑器";
			setLayout(new SoftBoxLayout(SoftBoxLayout.Y_AXIS));
			var topPanel : JPanel = new JPanel();
			var bottomPanel : JPanel = new JPanel();
			appendAll(topPanel, bottomPanel);
			initActionList(topPanel);
			initActionOperate(topPanel);
			initPreviewPanel(topPanel);
			initActionInfoList(bottomPanel);
			setupEvent();
		}

		private function setupEvent() : void {
			// 选择操作
			actionTable.addEventListener(SelectionEvent.ROW_SELECTION_CHANGED, onActionSelectedChanged);
			actionFrameList.addEventListener(SelectionEvent.LIST_SELECTION_CHANGED, onActionFrameSelectedChanged);
			allFrameList.addEventListener(SelectionEvent.LIST_SELECTION_CHANGED, onFrameSelectedChanged);
			allFrameList.addEventListener(GridListItemEvent.ITEM_DOUBLE_CLICK, onSelectedFrame);
			// 帧相关的操作
			addEmptyFrameToButton.addEventListener(MouseEvent.CLICK, onAddEmptyFrameTo);
			addFrameToButton.addEventListener(MouseEvent.CLICK, onAddFrameTo);
			delFrameFromButton.addEventListener(MouseEvent.CLICK, onDelFrameFrom);
			removeAllFrameFromButton.addEventListener(MouseEvent.CLICK, onRemoveAllFrameFrom);
			moveUpFrameButton.addEventListener(MouseEvent.CLICK, onMoveUpFrameButton);
			moveDownFrameButton.addEventListener(MouseEvent.CLICK, onMoveDownFrameButton);
			// 动作相关的操作
			addActionButton.addEventListener(MouseEvent.CLICK, onAddAction);
			delActionButton.addEventListener(MouseEvent.CLICK, onDelAction);
			copyActionButton.addEventListener(MouseEvent.CLICK, onCopyAction);
			removeAllActionButton.addEventListener(MouseEvent.CLICK, onRemoveAllAction);
		}

		private function onDelFrameFrom(event : MouseEvent) : void {
			removeFrameFromAction(getCurActionFrame(), getCurAction());
		}

		private function onSelectedFrame(event : GridListItemEvent) : void {
			onAddFrameTo(null);
		}

		private function onMoveDownFrameButton(event : MouseEvent) : void {
			var frame : ActionFrameInfo = getCurActionFrame();
			var pos : int = getCurAction().frameList.indexOf(frame);
			if (pos < getCurAction().frameList.length - 1) {
				changeFramePosition(getCurAction(), frame, pos + 1);
			}
		}

		private function onMoveUpFrameButton(event : MouseEvent) : void {
			var frame : ActionFrameInfo = getCurActionFrame();
			var pos : int = getCurAction().frameList.indexOf(frame);
			if (pos > 0) {
				changeFramePosition(getCurAction(), frame, pos - 1);
			}
		}

		private function onRemoveAllFrameFrom(event : MouseEvent) : void {
			var len : int = getCurAction().frameList.length;
			for (var i : int = len - 1;i >= 0;i--) {
				removeFrameFromAction(getCurAction().frameList[i], getCurAction());
			}
		}

		private function onAddEmptyFrameTo(event : Event) : void {
			var info : ActionFrameInfo = new ActionFrameInfo();
			info.frame = new EmptyFrameInfo();
			info.duration = 1;
			addActionFrame(info, getCurAction());
		}

		private function onAddFrameTo(event : MouseEvent) : void {
			var info : ActionFrameInfo = new ActionFrameInfo();
			info.frame = getCurFrame();
			info.duration = 1;
			addActionFrame(info, getCurAction());
		}

		private function onRemoveAllAction(event : MouseEvent) : void {
			var len : int = actionListModel.getSize();
			for (var i : int = len - 1;i >= 0;i--) {
				removeAction(actionListModel.getElementAt(i));
			}
		}

		private function onDelAction(event : MouseEvent) : void {
			removeAction(getCurAction());
		}

		private function onAddAction(event : MouseEvent) : void {
			addAction(null, null);
		}

		private function onCopyAction(event : MouseEvent) : void {
			addAction(getCurAction().name + "_Copy", getCurAction().frameList);
		}

		private function changeFramePosition(action : ActionInfo, frame : ActionFrameInfo, pos : int) : void {
			var event : ActionEditorEvent = new ActionEditorEvent(ActionEditorEvent.CHANGE_FRAME_POSITION);
			event.action = action;
			event.frame = frame;
			event.pos = pos;
			dispatchEvent(event);
		}

		private function removeAction(action : ActionInfo) : void {
			var event : ActionEditorEvent = new ActionEditorEvent(ActionEditorEvent.REMOVE_ACTION);
			event.action = action;
			dispatchEvent(event);
		}

		private function addAction(name : String, frameList : Array) : void {
			var action : ActionInfo = new ActionInfo();
			action.name = name || action.id.toString();
			action.frameList = frameList || [];
			var event : ActionEditorEvent = new ActionEditorEvent(ActionEditorEvent.ADD_ACTION);
			event.action = action;
			dispatchEvent(event);
		}

		private function addActionFrame(frame : ActionFrameInfo, action : ActionInfo) : void {
			var event : ActionEditorEvent = new ActionEditorEvent(ActionEditorEvent.ADD_FRAME_TO_ACTION);
			event.frame = frame;
			event.action = action;
			dispatchEvent(event);
		}

		private function removeFrameFromAction(frame : ActionFrameInfo, action : ActionInfo) : void {
			var event : ActionEditorEvent = new ActionEditorEvent(ActionEditorEvent.REMOVE_FRAME_FROM_ACTION);
			event.frame = frame;
			event.action = action;
			dispatchEvent(event);
		}

		/**
		 * 获取当前选择的动画
		 */
		public function getCurAction() : ActionInfo {
			return actionListModel.getElementAt(actionTable.getSelectionModel().getAnchorSelectionIndex());
		}

		/**
		 * 获取当前选择的动画帧
		 */
		public function getCurActionFrame() : ActionFrameInfo {
			return actionFrameList.getSelectedValue();
		}

		/**
		 * 获取当前选择的原始帧
		 */
		public function getCurFrame() : FrameInfo {
			return allFrameList.getSelectedValue();
		}

		private function onActionSelectedChanged(event : SelectionEvent) : void {
			var e : ActionEditorEvent = new ActionEditorEvent(ActionEditorEvent.SELECTED_ACTION_CHANGED);
			e.action = getCurAction();
			dispatchEvent(e);
			actionPreview.setAction(e.action);
		}

		private function onActionFrameSelectedChanged(event : SelectionEvent) : void {
			var e : ActionEditorEvent = new ActionEditorEvent(ActionEditorEvent.SELECTED_ACTIONFRAME_CHANGED);
			e.action = getCurAction();
			e.frame = getCurActionFrame();
			dispatchEvent(e);
		}

		private function onFrameSelectedChanged(event : SelectionEvent) : void {
			dispatchEvent(new Event(Event.CHANGE));
		}

		private function initActionOperate(container : Container) : void {
			var panel : JPanel = new JPanel();
			panel.setLayout(new SoftBoxLayout(SoftBoxLayout.Y_AXIS));
			panel.setBorder(new TitledBorder(null, "编辑动作"));
			panel.setPreferredWidth(300);
			panel.setPreferredHeight(415);
			var pane : JScrollPane = new JScrollPane(null, JScrollPane.SCROLLBAR_ALWAYS, JScrollPane.SCROLLBAR_NEVER);
			pane.setBorder(new BevelBorder());
			actionFrameList = new FrameEditorList();
			pane.append(actionFrameList);
			pane.setPreferredHeight(350);
			panel.append(pane);
			var panel2 : JPanel = new JPanel();
			panel2.append(addEmptyFrameToButton = new JButton("空帧"));
			panel2.append(addFrameToButton = new JButton("添加此帧"));
			panel2.append(delFrameFromButton = new JButton("移除此帧"));
			panel2.append(removeAllFrameFromButton = new JButton("清空所有帧"));
			panel2.append(moveUpFrameButton = new JButton("↑"));
			panel2.append(moveDownFrameButton = new JButton("↓"));
			panel.append(panel2);
			container.append(panel);
		}

		private function initPreviewPanel(container : Container) : void {
			var panel : JPanel = new JPanel();
			panel.setBorder(new TitledBorder(null, "预览"));
			panel.setPreferredWidth(300);
			panel.setPreferredHeight(415);
			actionPreview = new ActionPreview();
			actionPreview.setPreferredWidth(300);
			actionPreview.setPreferredHeight(300);
			panel.appendAll(actionPreview);
			container.append(panel);
		}

		private function initActionInfoList(container : Container) : void {
			var panel : JPanel = new JPanel();
			panel.setBorder(new TitledBorder(null, "Frame列表"));
			var pane : JScrollPane = new JScrollPane(null, JScrollPane.SCROLLBAR_NEVER);
			allFrameList = new FrameList();
			allFrameList.setHorizontalOnly(true);
			pane.append(allFrameList);
			pane.setPreferredWidth(750);
			pane.setPreferredHeight(100);
			panel.append(pane);
			container.append(panel);
		}

		private function initActionList(container : Container) : void {
			var panel : JPanel = new JPanel();
			panel.setBorder(new TitledBorder(null, "动作列表"));
			panel.setLayout(new SoftBoxLayout(SoftBoxLayout.Y_AXIS));
			var pane : JScrollPane = new JScrollPane();
			actionListModel = new VectorListModel();
			var model : PropertyTableModel = new PropertyTableModel(actionListModel, ["动作名", "帧数", "缩放"], ["name", "total_frame", "scale"], [null, null, null]);
			var sorter : TableSorter = new TableSorter(model);
			actionTable = new JTable(sorter);
			actionTable.getSelectionModel().setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			sorter.setTableHeader(actionTable.getTableHeader());
			sorter.setColumnSortable(2, false);
			sorter.setColumnSortable(1, false);
			model.setColumnEditable(1, false);
			pane.setPreferredWidth(300);
			pane.setPreferredHeight(350);
			pane.append(actionTable);
			panel.append(pane);
			var operatePanel : JPanel = new JPanel();
			operatePanel.append(addActionButton = new JButton("添加动作"));
			operatePanel.append(delActionButton = new JButton("删除动作"));
			operatePanel.append(copyActionButton = new JButton("复制动作"));
			operatePanel.append(removeAllActionButton = new JButton("移除全部动作"));
			panel.append(operatePanel);
			panel.setPreferredWidth(330);
			container.append(panel);
		}
	}
}
