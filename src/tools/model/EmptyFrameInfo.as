package tools.model {
	import flash.display.BitmapData;
	import flash.geom.Point;

	/**
	 * @author zhangming.luo
	 */
	public class EmptyFrameInfo extends FrameInfo {
		public function EmptyFrameInfo() {
			id = -1;
			$registration = new Point(0, 0);
		}

		override public function drawBitmap(canvas : BitmapData, destPosition : Point) : void {
			return;
		}

		override public function getBitmapData() : BitmapData {
			return new BitmapData(1, 1, true, 0x0);
		}
	}
}
