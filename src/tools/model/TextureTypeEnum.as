package tools.model
{
	import engine.datatype.ActionType;

	public class TextureTypeEnum
	{

		/**
		 * 站立
		 * */
		public static var IDLEl:String = "idle";
		/**
		 * 开火
		 * */
		public static var FIRE:String = "fire";
		/**
		 * 加血
		 * */
		public static var SUPPLY:String = "supply";
		/**
		 * 死亡(倒地)
		 * */
		public static var LAND:String = "land";
		/**
		 * 受伤
		 * */
		public static var DAMAGE:String = "damage";
		/**
		 * 移动
		 * */
		public static var WALK:String = "walk";
		/**
		 * 攻击动作1
		 * */
		public static var FIRE1:String = "fire1";
		/**
		 * 攻击动作2
		 * */
		public static var FIRE2:String = "fire2";
		/**
		 * 攻击动作3
		 * */
		public static var FIRE3:String = "fire3";
		/**
		 * 攻击动作4
		 * */
		public static var FIRE4:String = "fire4";
		/**
		 * 攻击动作5
		 * */
		public static var FIRE5:String = "fire5";
		
		/**
		 *  
		 */		
		public static const ACTION_STR : Array = [
			IDLEl, 
			FIRE, 
			SUPPLY,
			LAND,
			DAMAGE,
			WALK,
			FIRE1,
			FIRE2,
			FIRE3,
			FIRE4,
			FIRE5,
			IDLEl+"1", 
			FIRE+"1", 
			SUPPLY+"1",
			LAND+"1",
			DAMAGE+"1",
			WALK+"1",
		];
		
		/**
		 * 所有的类型  
		 */		
		public static const CONVENT_STR : Array = [
			"stills",
			"battles",
			"supply",
			"battles",
			"battles",
			"stills",
			"fire1",
			"others",
			"others",
			"others",
			"others",
			"stills",
			"battles",
			"supply",
			"battles",
			"battles",
			"stills",
		];
	}
}