package tools.model {
	import kiwi.util.sprintf;
	import kiwi.util.text.TextHelper;

	import flash.events.EventDispatcher;

	/**
	 * @author zhangming.luo
	 */
	public class BodyConfig extends EventDispatcher implements IConfigModel {
		public var id : int;
		public var name : String;
		public var url : String;
		public var effect_registration_x : int;
		public var effect_registration_y : int;
		public var exHorseUrl : String;
		public var type : String;

		public function postConstruct() : void {
			url = TextHelper.formatByArray("http://deploystatic.blessjoy.com/chronicleweb/asset/body/%s.ani", [id]);
			exHorseUrl = TextHelper.formatByArray("http://deploystatic.blessjoy.com/chronicleweb/asset/body/horse_%s.ani", [id]);
		}

		override public function toString() : String {
			return sprintf("%8d %s", id, name);
		}

		public function getAllState(horse : Boolean = false) : Array {
			return ["idle", "walk", "fire", "damage", "supply", "land", "fire1", "fire2", "fire3", "fire4", "fire5"];
		}

		public function getAllDirect() : Array {
			return ["free"];
		}
	}
}
