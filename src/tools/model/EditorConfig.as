package tools.model {
	import flash.events.EventDispatcher;
	import flash.filesystem.File;
	
	import kiwi.util.data.ValueChangeEvent;

	/**
	 * @author zhangming.luo
	 */
	public class EditorConfig extends EventDispatcher implements IConfigModel {
		public var name : String = "noname";
		public var quality : int = 80;
		private var _actionList : Array = [];
		private var _allFrameList : Array = [];
		private var _allTileList : Array = [];
		private var _texture : TextureBitmap = new TextureBitmap();
		private var _curAction : ActionInfo;
		private var _jdk : String;
		private var _flash : String;
		public var projectName : String = "noname";
		public var projectDir : File;

		public function get currentName() : File {
			return projectDir.resolvePath(projectName + ".sprite");
		}

		public function get actionList() : Array {
			return _actionList;
		}

		public function set actionList(value : Array) : void {
			var old : Array = _actionList;
			_actionList = value;
			dispatchEvent(new ValueChangeEvent("actionList", value, old, true));
		}

		public function get allFrameList() : Array {
			return _allFrameList;
		}

		public function set allFrameList(allFrameList : Array) : void {
			var old : Array = _allFrameList;
			_allFrameList = allFrameList;
			dispatchEvent(new ValueChangeEvent("allFrameList", allFrameList, old, true));
		}

		public function get allTileList() : Array {
			return _allTileList;
		}
		
		public function set allTileList(allTileList : Array) : void {
			var old : Array = _allTileList;
			_allTileList = allTileList;
			dispatchEvent(new ValueChangeEvent("allTileList", allTileList, old, true));
		}

		public function get texture() : TextureBitmap {
			return _texture;
		}

		public function set texture(texture : TextureBitmap) : void {
			var old : TextureBitmap = _texture;
			_texture = texture;
			dispatchEvent(new ValueChangeEvent("texture", texture, old));
		}
		
		public function get curAction() : ActionInfo {
			return _curAction;
		}

		public function set curAction(action : ActionInfo) : void {
			var old : ActionInfo = _curAction;
			_curAction = action;
			dispatchEvent(new ValueChangeEvent("curAction", action, old, true));
		}

		public function postConstruct() : void {
			texture.postConstruct();
		}

		public function get jdk() : String {
			return _jdk;
		}

		public function set jdk(jdk : String) : void {
			var file : File = File.applicationDirectory.resolvePath(jdk);
			if (!file.exists) {
				jdk = null;
			}
			var old : String = _jdk;
			_jdk = jdk;
			dispatchEvent(new ValueChangeEvent("jdk", jdk, old));
		}

		public function get flash() : String {
			return _flash;
		}

		public function set flash(flash : String) : void {
			_flash = flash;
		}
	}
}
