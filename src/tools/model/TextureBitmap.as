package tools.model {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.net.URLRequest;

	/**
	 * 纹理位图
	 * @author zhangming.luo
	 */
	public class TextureBitmap extends EventDispatcher implements IConfigModel {
		public var file : String;
		public var bitmapData : BitmapData;

		public function postConstruct() : void {
			var loader : Loader = new Loader();
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function(e : Event) : void {
				bitmapData = Bitmap(loader.contentLoaderInfo.content).bitmapData;
				dispatchEvent(new Event(Event.COMPLETE));
			});
			loader.load(new URLRequest(file));
		}
	}
}
 