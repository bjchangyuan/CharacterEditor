package tools.model {
	import kiwi.util.MysqlHelper;

	import flash.events.EventDispatcher;

	/**
	 * 数据库相关的配置
	 * 保证可以正常的从数据库中读取信息
	 * 进行编辑保存
	 * @author zhangming.luo
	 */
	public class DatabaseConfig extends EventDispatcher implements IConfigModel {
		public var dbServer : String = "10.0.0.207";
		public var dbPort : int = 3306;
		public var dbUsername : String = "root";
		public var dbPassword : String = "liumang";
		public var dbName : String = "webgame";
		/**
		 * 所有的角色
		 */
		public var bodyList : Array = [];

		public function postConstruct() : void {
		}
	}
}
