package tools.model {
	/**
	 * 一个完整的动画
	 * @author zhangming.luo
	 */
	public class ActionInfo {
		public static var cnt : int = 0;
		public var id : int = ++cnt;
		public var name : String = id.toString();
		public var frameList : Array = [];
		public var scale : Number = 1;

		public function get total_frame() : int {
			return frameList.length;
		}
	}
}
