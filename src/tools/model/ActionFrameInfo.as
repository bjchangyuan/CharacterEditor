package tools.model {
	/**
	 * @author zhangming.luo
	 */
	public class ActionFrameInfo {
		public var frame : FrameInfo;
		public var duration : int;

		public function toString() : String {
			return frame.toString();
		}
	}
}
