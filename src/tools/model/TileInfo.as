package tools.model {
	import kiwi.util.GeomUtils;
	import kiwi.util.sprintf;

	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	/**
	 * @author zhangming.luo
	 */
	public class TileInfo {
		public var id : int = -1;
		public var $cached : BitmapData;
		public var $source : TextureBitmap;
		public var name : String;
		public var source : Rectangle;
		public var registration : Point;

		public function getBitmapData() : BitmapData {
			var canvas : BitmapData = new BitmapData(source.width, source.height);
			canvas.copyPixels($source.bitmapData, source, GeomUtils.ZERO);
			return canvas;
		}

		public function toString() : String {
			return sprintf("%3d: %s", id, name);
		}
	}
}
