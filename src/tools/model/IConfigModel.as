package tools.model {
	import flash.events.IEventDispatcher;

	/**
	 * 需要保存的数据
	 * @author zhangming.luo
	 */
	public interface IConfigModel extends IEventDispatcher {
		function postConstruct() : void;
	}
}
