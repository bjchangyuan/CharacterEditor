package tools.model {
	/**
	 * 角色的基本数据
	 * @author zhangming.luo
	 */
	public class CharacterConfig {
		/**
		 * 名字
		 */
		public var name : String;
		/**
		 * 图片的地址
		 */
		public var img_url : String;
		/**
		 * 分块的位置
		 */
		public var tile_rect : Array;
		/**
		 * 帧列表
		 * 二维数组
		 * 内容为tile的列表
		 * 默认tile与frame对等
		 */
		public var frame_list : Array;
	}
}
