package tools.texture.controller {
	import tools.event.TextureEvent;
	import tools.model.EditorConfig;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author zhangming.luo
	 */
	public class UpdateFrameCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		[Inject]
		public var event : TextureEvent;

		override public function execute() : void {
			super.execute();
			event.frame.tileList = event.tileList;
			config.allFrameList = config.allFrameList;
		}
	}
}
