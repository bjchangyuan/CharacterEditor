package tools.texture.controller {
	import kiwi.util.ArrayUtils;

	import tools.event.TextureEvent;
	import tools.model.EditorConfig;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author zhangming.luo
	 */
	public class DeleteFrameCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		[Inject]
		public var event : TextureEvent;

		override public function execute() : void {
			super.execute();
			ArrayUtils.removeFromArray(config.allFrameList, event.frame);
			config.allFrameList = config.allFrameList;
		}
	}
}
