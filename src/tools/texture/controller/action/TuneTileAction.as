package tools.texture.controller.action {
	import kiwi.action.Action;
	import kiwi.debug.Log;
	import kiwi.util.bitmap.BitmapUtil;
	import kiwi.util.sprintf;

	import tools.model.TileInfo;

	import com.demonsters.debugger.MonsterDebugger;

	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.getTimer;

	/**
	 * 通过切分一个Tile使面积节约下来
	 * @author zhangming.luo
	 */
	public class TuneTileAction extends Action {
		public var newTiles : Array = [] ;
		public var tile : TileInfo;

		public function TuneTileAction(tile : TileInfo) {
			this.tile = tile;
		}

		override public function execute() : void {
			super.execute();
			var now : int = getTimer();
			var source : BitmapData = tile.getBitmapData();
			var a : * = getBest(source);
			var b : * = getBest(BitmapUtil.rotate90(source, 3));
			var best : int;
			if (a.size <= b.size) {
				// 纵向切割最好
				newTiles.push(newTile(new Rectangle(0, 0, a.index, source.height), tile.name + "#_left"));
				newTiles.push(newTile(new Rectangle(a.index, 0, source.width - a.index, source.height), tile.name + "#_right"));
				best = a.size;
			} else {
				// 横向切割最好
				newTiles.push(newTile(new Rectangle(0, 0, source.width, b.index), tile.name + "#_up"));
				newTiles.push(newTile(new Rectangle(0, b.index, source.width, source.height - b.index), tile.name + "#_down"));
				best = b.size;
			}
			Log.info(sprintf("%10s (%5d)=>(%5.2f%%) Cost %d", tile.name, (tile.source.width * tile.source.height) * 4, best * 100 / ((tile.source.width * tile.source.height) * 4), getTimer() - now));
			result();
		}

		private function newTile(clip : Rectangle, name : String) : TileInfo {
			var info : TileInfo = new TileInfo();
			info.name = name;
			info.$source = tile.$source;
			info.source = new Rectangle(tile.source.x + clip.x, tile.source.y + clip.y, clip.width, clip.height);
			info.registration = new Point(tile.registration.x - clip.x, tile.registration.y - clip.y);
			var trimBounds : Rectangle = new Rectangle();
			BitmapUtil.trim(info.getBitmapData(), trimBounds);
			info.source = new Rectangle(info.source.x + trimBounds.x, info.source.y + trimBounds.y, trimBounds.width, trimBounds.height);
			info.registration = new Point(info.registration.x - trimBounds.x, info.registration.y - trimBounds.y);
			return info;
		}

		private function getBest(source : BitmapData) : * {
			var min : int = int.MAX_VALUE;
			var best : int = -1;
			var bestX : int = -1;
			for (var x : int = 1;x < source.width;x++) {
				var size : int = getSize([BitmapUtil.clip(source, new Rectangle(0, 0, x, source.height), false), BitmapUtil.clip(source, new Rectangle(x, 0, source.width - x, source.height), false)]);
				if (size < min) {
					best = size;
					bestX = x;
					min = size;
				}
			}
			return {size:best, index:bestX};
		}

		private function getSize(bitmaps : Array) : int {
			var sum : int = 0;
			for each (var bitmap:BitmapData in bitmaps) {
				var newBitmap : BitmapData = BitmapUtil.trim(bitmap, null, false);
				sum += (newBitmap.width * newBitmap.height) * 4;
			}
			return sum;
		}
	}
}

