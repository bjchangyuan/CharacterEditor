package tools.texture.controller {
	import kiwi.util.ArrayUtils;

	import tools.model.FrameInfo;

	import org.aswing.plaf.basic.icon.FrameIcon;

	import tools.event.TextureEvent;
	import tools.model.EditorConfig;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author zhangming.luo
	 */
	public class NewFrameCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		[Inject]
		public var event : TextureEvent;

		override public function execute() : void {
			super.execute();
			var frame : FrameInfo = new FrameInfo();
			frame.id = ArrayUtils.max(ArrayUtils.collect(config.allFrameList, "id")) + 1;
			frame.name = String(frame.id);
			frame.tileList = event.tileList;
			frame.$source = config.texture;
			config.allFrameList.push(frame);
			config.allFrameList = config.allFrameList;
		}
	}
}
