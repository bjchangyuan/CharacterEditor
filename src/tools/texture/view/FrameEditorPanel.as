package tools.texture.view {
	import tools.event.TextureEvent;

	import flash.display3D.textures.Texture;

	import org.aswing.border.BevelBorder;

	import tools.event.ProjectEvent;
	import tools.global.view.FrameList;

	import org.aswing.JButton;
	import org.aswing.JLabel;
	import org.aswing.JList;
	import org.aswing.JPanel;
	import org.aswing.JScrollPane;
	import org.aswing.SoftBoxLayout;
	import org.aswing.border.TitledBorder;

	import flash.events.MouseEvent;

	/**
	 * @author zhangming.luo
	 */
	public class FrameEditorPanel extends JPanel {
		public var tileList : JList;
		public var frameList : FrameList;
		public var framePreviewContainer : PreviewContainer;
		public var previewContainer : PreviewContainer;
		public var browseTextureButton : JButton;
		public var browseConfigButton : JButton;
		public var tilePreview : TilePreviewContainer;
		private var contentPanel : JPanel;
		private var newFrameButton : JButton;
		private var updateFrameButton : JButton;
		private var deleteFrameButton : JButton;

		public function FrameEditorPanel() {
			name = "帧编辑器";
			setLayout(new SoftBoxLayout(SoftBoxLayout.Y_AXIS));
			initTop();
			initContent();
			initFrame();
			initTile();
			setupEvent();
		}

		private function setupEvent() : void {
			browseTextureButton.addEventListener(MouseEvent.CLICK, onBrowseTextureButton);
			browseConfigButton.addEventListener(MouseEvent.CLICK, onBrowseConfigButton);
			newFrameButton.addEventListener(MouseEvent.CLICK, onNewFrameButton);
			updateFrameButton.addEventListener(MouseEvent.CLICK, onUpdateFrameButton);
			deleteFrameButton.addEventListener(MouseEvent.CLICK, onDeleteFrameButton);
		}

		private function onDeleteFrameButton(event : MouseEvent) : void {
			var e : TextureEvent = new TextureEvent(TextureEvent.DELETE_FRAME);
			e.frame = frameList.getSelectedValue();
			dispatchEvent(e);
		}

		private function onUpdateFrameButton(event : MouseEvent) : void {
			var e : TextureEvent = new TextureEvent(TextureEvent.UPDATE_FRAME);
			e.frame = frameList.getSelectedValue();
			e.tileList = tilePreview.getTileList();
			dispatchEvent(e);
		}

		private function onNewFrameButton(event : MouseEvent) : void {
			var e : TextureEvent = new TextureEvent(TextureEvent.NEW_FRAME);
			e.tileList = tilePreview.getTileList();
			dispatchEvent(e);
		}

		private function onBrowseConfigButton(event : MouseEvent) : void {
			dispatchEvent(new ProjectEvent(ProjectEvent.BROWSE_TEXTURE_CONFIG));
		}

		private function onBrowseTextureButton(event : MouseEvent) : void {
			dispatchEvent(new ProjectEvent(ProjectEvent.BROWSE_TEXTURE_IMG));
		}

		private function initFrame() : void {
			var panel : JPanel = new JPanel();
			panel.setLayout(new SoftBoxLayout(SoftBoxLayout.Y_AXIS));
			framePreviewContainer = new PreviewContainer();
			framePreviewContainer.setPreferredWidth(512);
			framePreviewContainer.setPreferredHeight(512);
			frameList = new FrameList();
			frameList.setHorizontalOnly(true);
			var pane : JScrollPane = new JScrollPane(null, JScrollPane.SCROLLBAR_NEVER, JScrollPane.SCROLLBAR_ALWAYS);
			pane.setBorder(new TitledBorder(null, "帧列表"));
			pane.append(frameList);
			pane.setPreferredHeight(125);
			panel.appendAll(framePreviewContainer, pane);
			panel.setPreferredWidth(512);
			contentPanel.append(panel);
		}

		private function initTile() : void {
			var panel : JPanel = new JPanel();
			panel.setPreferredWidth(200);
			tilePreview = new TilePreviewContainer();
			tilePreview.setPreferredWidth(200);
			tilePreview.setPreferredHeight(300);
			tilePreview.setBorder(new BevelBorder());
			var frameOperatePanel : JPanel = new JPanel();
			frameOperatePanel.appendAll(newFrameButton = new JButton("添加帧"), updateFrameButton = new JButton("更新帧"), deleteFrameButton = new JButton("删除帧"));
			var pane : JScrollPane;
			panel.setLayout(new SoftBoxLayout(SoftBoxLayout.Y_AXIS));
			pane = new JScrollPane(null, 0, JScrollPane.SCROLLBAR_NEVER);
			pane.setBorder(new TitledBorder(null, "Tile列表"));
			pane.append(tileList = new JList());
			pane.setPreferredHeight(250);
			panel.appendAll(tilePreview, frameOperatePanel, pane);
			contentPanel.append(panel);
		}

		private function initTop() : void {
			var panel : JPanel = new JPanel();
			panel.append(new JLabel("图片路径:"));
			browseTextureButton = new JButton("  打开图片   ");
			browseConfigButton = new JButton("  打开配置   ");
			panel.appendAll(browseTextureButton, browseConfigButton);
			append(panel);
		}

		private function initContent() : void {
			contentPanel = new JPanel();
			contentPanel.setLayout(new SoftBoxLayout(SoftBoxLayout.X_AXIS));
			append(contentPanel);
		}
	}
}
