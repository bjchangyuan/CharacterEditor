package tools.texture.view {
	import org.robotlegs.mvcs.Mediator;


	/**
	 * 
	 * @author zhangming.luo
	 */
	public class PreviewMediator extends Mediator {
		[Inject]
		public var view : PreviewContainer;

		override public function onRegister() : void {
			super.onRegister();
		}
	}
}
