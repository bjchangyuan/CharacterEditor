package tools.texture.view {
	import flash.filters.GlowFilter;
	import flash.display.DisplayObject;

	import org.aswing.event.SelectionEvent;

	import kiwi.util.DisplayObjectUtils;

	import tools.global.view.SortEvent;
	import tools.global.view.SortableCell;
	import tools.model.TileInfo;

	import org.aswing.Container;
	import org.aswing.DefaultListSelectionModel;
	import org.aswing.GeneralListCellFactory;
	import org.aswing.JList;
	import org.aswing.JPanel;
	import org.aswing.JScrollPane;
	import org.aswing.SoftBoxLayout;
	import org.aswing.border.BevelBorder;
	import org.aswing.border.TitledBorder;

	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Rectangle;

	/**
	 * @author zhangming.luo
	 */
	public class TilePreviewContainer extends JPanel {
		private var preview : Container = new Container();
		private var container : Sprite = new Sprite();
		private var _tileList : Array;
		private var tileList : JList;

		public function TilePreviewContainer() {
			setLayout(new SoftBoxLayout(SoftBoxLayout.Y_AXIS));
			preview.setPreferredWidth(200);
			preview.setPreferredHeight(200);
			preview.addChild(container);
			container.x = 100;
			container.y = 100;
			append(preview);
			var pane : JScrollPane = new JScrollPane(null, JScrollPane.SCROLLBAR_AS_NEEDED, JScrollPane.SCROLLBAR_NEVER);
			pane.setBorder(new TitledBorder());
			pane.append(tileList = new JList(null, new GeneralListCellFactory(SortableCell)));
			tileList.getSelectionModel().setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);
			tileList.setBorder(new BevelBorder());
			pane.setPreferredHeight(100);
			append(pane);
			tileList.addEventListener(SortEvent.CHANGE, onChange);
			tileList.addEventListener(SelectionEvent.LIST_SELECTION_CHANGED, onTileSelected);
		}

		private function onTileSelected(event : SelectionEvent) : void {
			for (var i : int = 0;i < container.numChildren;i++) {
				var canvas : DisplayObject = container.getChildAt(i);
				if (i == tileList.getSelectionModel().getAnchorSelectionIndex()) {
					canvas.filters = [new GlowFilter(0xff00ff)];
				} else {
					canvas.filters = null;
				}
			}
		}

		private function onChange(event : SortEvent) : void {
			var oldIndex : int = event.oldIndex;
			var newIndex : int = Math.max(0, Math.min(event.newIndex, _tileList.length - 1));

			if (oldIndex == newIndex) {
				return ;
			}
			var model : TileInfo = _tileList[oldIndex];
			_tileList.splice(oldIndex, 1);
			if (oldIndex > newIndex) {
				_tileList.splice(newIndex, -1, model);
			} else {
				_tileList.splice(newIndex, -1, model);
			}
			tileList.setListData(_tileList);
			showFrame();
			tileList.setSelectedIndex(newIndex);
			tileList.updateUI();
		}

		public function getTileList() : Array {
			return _tileList;
		}

		public function setTileList(value : Array) : void {
			if (!value || value.length == 0) {
				return;
			}
			_tileList = value.concat();
			tileList.setListData(_tileList);
			showFrame();
		}

		private function showFrame() : void {
			DisplayObjectUtils.cleanSprite(container);
			for each (var tile:TileInfo in _tileList) {
				var canvas : Bitmap = new Bitmap(tile.getBitmapData());
				canvas.x = -tile.registration.x;
				canvas.y = -tile.registration.y;
				container.addChild(canvas);
			}
			fit();
		}

		private function fit() : void {
			var bounds : Rectangle = container.getBounds(container);
			if (Math.max(- bounds.left, bounds.right) > Math.max(- bounds.top, bounds.bottom)) {
				container.scaleX = preview.getPreferredWidth() / 2 / Math.max(- bounds.left, bounds.right);
				container.scaleY = container.scaleX;
			} else {
				container.scaleY = preview.getPreferredHeight() / 2 / Math.max(- bounds.top, bounds.bottom);
				container.scaleX = container.scaleY;
			}
		}
	}
}
