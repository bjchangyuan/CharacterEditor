package tools.event {
	import flash.events.Event;

	/**
	 * @author zhangming.luo
	 */
	public class ApplicationEvent extends Event {
		static public const CONFIG_VIEW : String = "ApplicationEvent.configView";
		static public const CONFIG_MODEL : String = "ApplicationEvent.configModel";
		static public const CONFIG_SERVICE : String = "ApplicationEvent.configService";
		static public const CONFIG_CONTROLLER : String = "ApplicationEvent.configController";
		public static const EXIT : String = "EXIT";
		public static const CONFIG_JDK : String = "CONFIG";
		public static const CONFIG_FLASH : String = "CONFIG_FLASH";

		public function ApplicationEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false) {
			super(type, bubbles, cancelable);
		}
	}
}
