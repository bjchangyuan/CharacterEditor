package tools.event {
	import flash.events.Event;
	import flash.filesystem.File;

	/**
	 * @author zhangming.luo
	 */
	public class ProjectEvent extends Event {
		public static const PUBLISH : String = "PUBLISH";
		public static const SAVE : String = "SAVE";
		public static const SAVE_TEMP : String = "SAVE_TEMP";
		public static const LOAD : String = "LOAD";
		public static const IMPORT : String = "IMPORT";
		public static const BROWSE_TEXTURE_CONFIG : String = "BROWSE_TEXTURE_CONFIG";
		public static const BROWSE_TEXTURE_IMG : String = "BROWSE_TEXTURE_IMG";
		public static const OPEN_TEXTURE_CONFIG : String = "OPEN_TEXTURE_CONFIG";
		public static const OPEN_TEXTURE_IMG : String = "OPEN_TEXTURE_IMG";
		public static const TEXTURE_IMG_LOADED : String = "TEXTURE_IMG_LOADED";
		public static const NEW : String = "NEW";
		public static const BATCH_PUBLISH : String = "BATCH_PUBLISH";
		public static const PUBLISH_COMPLETE : String = "PUBLISH_COMPLETE";
		public static const IMPORT_COMPLETE : String = "IMPORT_COMPLETE";
		public static const LOAD_COMPLETE : String = "LOAD_COMPLETE";
		public static const TRANS : String = "TRANS";
		public static const EXPORT : String = "EXPORT";
		public static const JSFL2SWF : String = "JSFL2SWF";
		public static const ANI2ANI : String = "ANI2ANI";
		public static const ANITEST : String = "ANITEST";
		public static const CLEANAS : String = "CLEANAS";
		public static const PUBLISH_FOR_MOBILE : String = "PUBLISH_FOR_MOBILE";
		public var file : File;
		public var ignorInfo : Boolean;
		public var model : *;

		public function ProjectEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false, file : File = null, ignorInfo : Boolean = false, model : * = null) {
			this.model = model;
			this.ignorInfo = ignorInfo;
			this.file = file;
			super(type, bubbles, cancelable);
		}
	}
}
