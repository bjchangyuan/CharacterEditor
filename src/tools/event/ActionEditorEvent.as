package tools.event {
	import tools.model.ActionFrameInfo;
	import tools.model.ActionInfo;

	import flash.events.Event;

	/**
	 * @author zhangming.luo
	 */
	public class ActionEditorEvent extends Event {
		public static const ADD_FRAME_TO_ACTION : String = "ADD_FRAME_TO_ACTION";
		public static const REMOVE_FRAME_FROM_ACTION : String = "REMOVE_FRAME_FROM_ACTION";
		public static const ADD_ACTION : String = "ADD_ACTION";
		public static const REMOVE_ACTION : String = "REMOEV_ACTION";
		public static const CHANGE_FRAME_POSITION : String = "CHANGE_FRAME_POSITION";
		public static const SHOW_MAIN : String = "SHOW_MAIN";
		public static const SELECTED_ACTION_CHANGED : String = "SELECTED_ACTION_CHANGED";
		public static const SELECTED_ACTIONFRAME_CHANGED : String = "SELECTED_ACTIONFRAME_CHANGED";
		public var action : ActionInfo;
		public var frame : ActionFrameInfo;
		public var pos : int;

		public function ActionEditorEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false) {
			super(type, bubbles, cancelable);
		}

		public override function clone() : Event {
			var event : ActionEditorEvent = new ActionEditorEvent(type, bubbles, cancelable);
			event.action = this.action;
			event.frame = this.frame;
			event.pos = this.pos;
			return event;
		}
	}
}
