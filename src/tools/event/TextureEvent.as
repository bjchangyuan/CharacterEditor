package tools.event {
	import tools.model.FrameInfo;

	import flash.events.Event;

	/**
	 * @author zhangming.luo
	 */
	public class TextureEvent extends Event {
		public static const DELETE_FRAME : String = "TextureEvent::DELETE_FRAME";
		public static const NEW_FRAME : String = "TextureEvent::NEW_FRAME";
		public static const UPDATE_FRAME : String = "TextureEvent::UPDATE_FRAME";
		public static const TUNE : String = "TextureEvent::TUNE";
		public static const REBUILD : String = "TextureEvent::REBUILD";
		public var frame : FrameInfo;
		public var tileList : Array;

		public function TextureEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false) {
			super(type, bubbles, cancelable);
		}

		override public function clone() : Event {
			var event : TextureEvent = new TextureEvent(type, bubbles, cancelable);
			event.frame = frame;
			event.tileList = tileList;
			return event;
		}
	}
}
