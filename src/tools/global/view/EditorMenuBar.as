package tools.global.view {
	import org.aswing.JMenu;
	import org.aswing.JMenuBar;
	import org.aswing.JMenuItem;

	/**
	 * @author zhangming.luo
	 */
	public class EditorMenuBar extends JMenuBar {
		public var openProjectItem : JMenuItem;
		public var exitItem : JMenuItem;
		public var saveProjectItem : JMenuItem;
		public var publishProjectItem : JMenuItem;
		public var publishProjectForMobileItem : JMenuItem;
		public var tuneTextureItem : JMenuItem;
		public var rebuildTextureItem : JMenuItem;
		public var configJDKItem : JMenuItem;
		public var configFlashItem : JMenuItem;
		public var importProjectItem : JMenuItem;
		public var batchItem : JMenuItem;
		public var transItem : JMenuItem;
		public var exportItem : JMenuItem;
		public var jsfl2swf:JMenuItem;
		public var ani2ani:JMenuItem;
		public var oneKeyTest : JMenuItem;
		public var cleanAS : JMenuItem;

		public function EditorMenuBar() {
			var fileMenu : JMenu = new JMenu("文件");
			fileMenu.append(openProjectItem = new JMenuItem("打开项目..."));
			fileMenu.append(saveProjectItem = new JMenuItem("保存"));
			fileMenu.append(publishProjectItem = new JMenuItem("发布"));
			fileMenu.append(publishProjectForMobileItem=new JMenuItem("发布给手机"));
			fileMenu.append(exitItem = new JMenuItem("退出"));
			var textureMenu : JMenu = new JMenu("素材");
			textureMenu.append(tuneTextureItem = new JMenuItem("优化素材"));
			textureMenu.append(rebuildTextureItem = new JMenuItem("重组素材"));
			var toolMenu : JMenu = new JMenu("工具");
			toolMenu.append(configJDKItem = new JMenuItem("配置JDK"));
			toolMenu.append(configFlashItem = new JMenuItem("配置Flash"));
			toolMenu.append(batchItem = new JMenuItem("批量发布"));
			toolMenu.append(importProjectItem = new JMenuItem("从文件夹导入"));
			toolMenu.append(transItem = new JMenuItem("批量转转"));
			toolMenu.append(exportItem = new JMenuItem("批量导出"));
			toolMenu.append(jsfl2swf = new JMenuItem("jsfl2swf"));
			toolMenu.append(ani2ani = new JMenuItem("ani2ani"));
			toolMenu.append(oneKeyTest = new JMenuItem("ani测试"));
			toolMenu.append(cleanAS = new JMenuItem("cleanAS"));
			
			appendAll(fileMenu, textureMenu, toolMenu);
		}
	}
}
