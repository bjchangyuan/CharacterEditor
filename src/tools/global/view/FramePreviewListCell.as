package tools.global.view {
	import tools.model.FrameInfo;

	import org.aswing.ASColor;
	import org.aswing.Component;
	import org.aswing.Container;
	import org.aswing.JLabel;
	import org.aswing.JPanel;
	import org.aswing.border.BevelBorder;
	import org.aswing.ext.GridList;
	import org.aswing.ext.GridListCell;

	import flash.display.Bitmap;

	/**
	 * @author zhangming.luo
	 */
	public class FramePreviewListCell implements GridListCell {
		private var frameInfo : FrameInfo;
		private var panel : JPanel;
		private var canvas : Bitmap;
		private var frameLabel : JLabel;

		public function setCellValue(value : *) : void {
			frameInfo = value as FrameInfo;
			getPreview();
			if (frameInfo) {
				canvas.bitmapData = frameInfo.getBitmapData();
				canvas.width = 40;
				canvas.scaleY = canvas.scaleX;
				frameLabel.setText(frameInfo.toString());
			}
		}

		public function getCellComponent() : Component {
			return getPreview();
		}

		public function getPreview() : Component {
			if (!panel) {
				panel = new JPanel();
				panel.setOpaque(true);
				panel.setFocusable(false);
				panel.setBorder(new BevelBorder());
				frameLabel = new JLabel("");
				frameLabel.setPreferredHeight(15);
				panel.append(frameLabel);
				var container : Container = new Container();
				canvas = new Bitmap();
				container.addChild(canvas);
				container.setPreferredWidth(50);
				container.setPreferredHeight(50);
				panel.append(container);
				panel.doubleClickEnabled = true;
				frameLabel.doubleClickEnabled = true;
				container.doubleClickEnabled = true;
			}
			return panel;
		}

		public function setGridListCellStatus(gridList : GridList, selected : Boolean, index : int) : void {
			var c : ASColor = index % 2 == 0 ? ASColor.WHITE : ASColor.GRAY;
			if (selected) {
				c = ASColor.BLUE;
			}
			panel.setBackground(c);
		}

		public function getCellValue() : * {
			return frameInfo;
		}
	}
}
