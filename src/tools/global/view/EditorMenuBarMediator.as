package tools.global.view {
	import flash.events.MouseEvent;
	import flash.filesystem.File;

	import org.robotlegs.mvcs.Mediator;

	import tools.event.ApplicationEvent;
	import tools.event.ProjectEvent;
	import tools.event.TextureEvent;
	import tools.utils.FileHelper;

	/**
	 * @author zhangming.luo
	 */
	public class EditorMenuBarMediator extends Mediator {
		[Inject]
		public var view : EditorMenuBar;

		override public function onRegister() : void {
			super.onRegister();
			view.openProjectItem.addEventListener(MouseEvent.CLICK, dispatchOpenProject);
			view.publishProjectItem.addEventListener(MouseEvent.CLICK, dispatchExportProject);
			view.publishProjectForMobileItem.addEventListener(MouseEvent.CLICK, dispatchExportProjectForMobile);
			view.saveProjectItem.addEventListener(MouseEvent.CLICK, dispatchSaveProject);
			view.exitItem.addEventListener(MouseEvent.CLICK, dispatchExit);
			view.tuneTextureItem.addEventListener(MouseEvent.CLICK, dispatchTuneTexture);
			view.rebuildTextureItem.addEventListener(MouseEvent.CLICK, dispachRebuildTexture);
			view.configJDKItem.addEventListener(MouseEvent.CLICK, dispatchConfigJDK);
			view.configFlashItem.addEventListener(MouseEvent.CLICK, dispatchConfigFlash);
			view.importProjectItem.addEventListener(MouseEvent.CLICK, dispatchImport);
			view.batchItem.addEventListener(MouseEvent.CLICK, dispatchBatch);
			view.transItem.addEventListener(MouseEvent.CLICK, dispatchTrans);
			view.exportItem.addEventListener(MouseEvent.CLICK, dispatchExport);
			view.jsfl2swf.addEventListener(MouseEvent.CLICK, jsfl2swfHandle);
			view.ani2ani.addEventListener(MouseEvent.CLICK, ani2aniHandle);
			view.oneKeyTest.addEventListener(MouseEvent.CLICK, oneKeyTest);
			view.cleanAS.addEventListener(MouseEvent.CLICK, cleanAS);
		}

		private function dispatchExportProjectForMobile(event : MouseEvent) : void {
			dispatch(new ProjectEvent(ProjectEvent.PUBLISH_FOR_MOBILE));
		}

		private function dispatchExport(event : MouseEvent) : void {
			dispatch(new ProjectEvent(ProjectEvent.EXPORT));
		}

		private function dispatchTrans(event : MouseEvent) : void {
			dispatch(new ProjectEvent(ProjectEvent.TRANS));
		}

		private function dispatchBatch(event : MouseEvent) : void {
			dispatch(new ProjectEvent(ProjectEvent.BATCH_PUBLISH));
		}

		private function dispatchImport(event : MouseEvent) : void {
			dispatch(new ProjectEvent(ProjectEvent.IMPORT));
		}

		private function dispatchConfigJDK(event : MouseEvent) : void {
			dispatch(new ApplicationEvent(ApplicationEvent.CONFIG_JDK));
		}

		private function dispatchConfigFlash(event : MouseEvent) : void {
			dispatch(new ApplicationEvent(ApplicationEvent.CONFIG_FLASH));
		}

		private function dispachRebuildTexture(event : MouseEvent) : void {
			dispatch(new TextureEvent(TextureEvent.REBUILD));
		}

		private function dispatchTuneTexture(event : MouseEvent) : void {
			dispatch(new TextureEvent(TextureEvent.TUNE));
		}

		private function dispatchExit(event : MouseEvent) : void {
			dispatch(new ApplicationEvent(ApplicationEvent.EXIT));
		}

		private function dispatchOpenProject(event : MouseEvent) : void {
			var file : File = FileHelper.loadFile(function() : void {
				var e : ProjectEvent = new ProjectEvent(ProjectEvent.LOAD);
				e.file = file;
				dispatch(e);
			}, FileHelper.filters("sprite"));
		}

		private function dispatchSaveProject(event : MouseEvent) : void {
			dispatch(new ProjectEvent(ProjectEvent.SAVE));
		}

		private function dispatchExportProject(event : MouseEvent) : void {
			dispatch(new ProjectEvent(ProjectEvent.PUBLISH));
		}

		private function jsfl2swfHandle(event : MouseEvent) : void {
			dispatch(new ProjectEvent(ProjectEvent.JSFL2SWF));
		}

		private function ani2aniHandle(event : MouseEvent) : void {
			dispatch(new ProjectEvent(ProjectEvent.ANI2ANI));
		}

		private function oneKeyTest(event : MouseEvent) : void {
			dispatch(new ProjectEvent(ProjectEvent.ANITEST));
		}

		private function cleanAS(event : MouseEvent) : void {
			dispatch(new ProjectEvent(ProjectEvent.CLEANAS));
		}
	}
}
