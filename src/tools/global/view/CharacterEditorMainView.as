package tools.global.view {
	import tools.body.view.BodyEditorPanel;
	import tools.action.view.ActionEditorPanel;
	import tools.texture.view.FrameEditorPanel;

	import org.aswing.ASColor;
	import org.aswing.JLabel;
	import org.aswing.JPanel;
	import org.aswing.JTabbedPane;
	import org.aswing.JWindow;
	import org.aswing.SoftBoxLayout;
	import org.aswing.SolidBackground;

	import flash.display.Sprite;

	/**
	 * @author zhangming.luo
	 */
	public class CharacterEditorMainView extends Sprite {
		private var window : JWindow;
		private var mainMenuBar : EditorMenuBar;
		private var tabPane : JTabbedPane;
		private var mainPanel : JPanel;
		private var statusBar : JLabel;

		public function CharacterEditorMainView() {
			window = new JWindow(this);
			window.x = 0;
			window.y = 0;
			mainPanel = new JPanel();
			mainPanel.setLayout(new SoftBoxLayout(SoftBoxLayout.Y_AXIS));
			initMenu();
			initTab();
			mainPanel.append(mainMenuBar);
			mainPanel.append(tabPane);
			mainPanel.append(statusBar = new JLabel());
			window.setContentPane(mainPanel);
			window.setBackgroundDecorator(new SolidBackground(ASColor.LIGHT_GRAY));
			window.setSizeWH(1200, 800);
			window.show();
		}

		private function initTab() : void {
			tabPane = new JTabbedPane();
			tabPane.append(new FrameEditorPanel());
			tabPane.append(new ActionEditorPanel());
			tabPane.append(new BodyEditorPanel());
		}

		private function initMenu() : void {
			mainMenuBar = new EditorMenuBar();
		}
	}
}
