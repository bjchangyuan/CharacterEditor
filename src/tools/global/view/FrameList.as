package tools.global.view {
	import org.aswing.ext.GeneralGridListCellFactory;
	import org.aswing.ext.GridList;

	/**
	 * @author zhangming.luo
	 */
	public class FrameList extends GridList {
		private var _verticalOnly : Boolean;
		private var _horizontalOnly : Boolean;

		public function FrameList() {
			super(null, new GeneralGridListCellFactory(FramePreviewListCell));
			setTileWidth(85);
			setTileHeight(85);
		}

		public function setHorizontalOnly(value : Boolean) : void {
			_horizontalOnly = value;
			updateUI();
		}

		public function setVerticalOnly(value : Boolean) : void {
			_verticalOnly = value;
			updateUI();
		}

		override public function updateUI() : void {
			if (_horizontalOnly) {
				// 水平的
				setRows(1);
			} else if (_verticalOnly) {
				setColumns(1);
				setRows(model.getSize());
			}
			super.updateUI();
		}
	}
}
