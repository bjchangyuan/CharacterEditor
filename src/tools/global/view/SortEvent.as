package tools.global.view {
	import flash.events.Event;

	/**
	 * @author zhangming.luo
	 */
	public class SortEvent extends Event {
		public static const CHANGE : String = "SORT::CHANGE";
		public var oldIndex : int;
		public var newIndex : int;

		public function SortEvent(type : String, bubbles : Boolean = false, cancelable : Boolean = false) {
			super(type, bubbles, cancelable);
		}
	}
}
