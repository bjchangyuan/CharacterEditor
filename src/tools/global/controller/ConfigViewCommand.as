package tools.global.controller {
	import tools.body.view.BodyEditorMediator;
	import tools.body.view.BodyEditorPanel;

	import org.robotlegs.mvcs.Command;

	import tools.action.view.ActionEditorMediator;
	import tools.action.view.ActionEditorPanel;
	import tools.event.ActionEditorEvent;
	import tools.global.view.EditorMenuBar;
	import tools.global.view.EditorMenuBarMediator;
	import tools.texture.view.FrameEditorMediator;
	import tools.texture.view.FrameEditorPanel;
	import tools.texture.view.PreviewContainer;
	import tools.texture.view.PreviewMediator;

	/**
	 * @author zhangming.luo
	 */
	public class ConfigViewCommand extends Command {
		override public function execute() : void {
			mediatorMap.mapView(ActionEditorPanel, ActionEditorMediator);
			mediatorMap.mapView(PreviewContainer, PreviewMediator);
			mediatorMap.mapView(FrameEditorPanel, FrameEditorMediator);
			mediatorMap.mapView(EditorMenuBar, EditorMenuBarMediator);
			mediatorMap.mapView(BodyEditorPanel, BodyEditorMediator);
			dispatch(new ActionEditorEvent(ActionEditorEvent.SHOW_MAIN));
		}
	}
}
