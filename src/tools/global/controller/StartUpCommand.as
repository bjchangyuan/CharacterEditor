package tools.global.controller {
	import tools.event.ApplicationEvent;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author zhangming.luo
	 */
	public class StartUpCommand extends Command {
		override public function execute() : void {
			commandMap.mapEvent(ApplicationEvent.CONFIG_CONTROLLER, ConfigControllerCommand, ApplicationEvent, true);
			commandMap.mapEvent(ApplicationEvent.CONFIG_SERVICE, ConfigServiceCommand, ApplicationEvent, true);
			commandMap.mapEvent(ApplicationEvent.CONFIG_MODEL, ConfigModelCommand, ApplicationEvent, true);
			commandMap.mapEvent(ApplicationEvent.CONFIG_VIEW, ConfigViewCommand, ApplicationEvent, true);
			commandMap.mapEvent(ApplicationEvent.EXIT, ExitCommand, ApplicationEvent, true);
			commandMap.mapEvent(ApplicationEvent.CONFIG_JDK, ConfigJDKCommand, ApplicationEvent);
			commandMap.mapEvent(ApplicationEvent.CONFIG_FLASH, ConfigFlashCommand, ApplicationEvent);

			dispatch(new ApplicationEvent(ApplicationEvent.CONFIG_CONTROLLER));
		}
	}
}
