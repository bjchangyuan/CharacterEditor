package tools.global.controller {
	import tools.project.controller.PublishProjectForMobileCommand;

	import org.robotlegs.mvcs.Command;

	import tools.action.controller.AddActionCommand;
	import tools.action.controller.AddFrameToActionCommand;
	import tools.action.controller.ChangeFramePositionCommand;
	import tools.action.controller.RemoveActionCommand;
	import tools.action.controller.RemoveFrameFromActionCommand;
	import tools.event.ActionEditorEvent;
	import tools.event.ApplicationEvent;
	import tools.event.ProjectEvent;
	import tools.event.TextureEvent;
	import tools.project.controller.Ani2AniCommand;
	import tools.project.controller.AniTestCommand;
	import tools.project.controller.BatchPublishCommand;
	import tools.project.controller.BrowseTextureConfCommand;
	import tools.project.controller.BrowseTextureImgCommand;
	import tools.project.controller.CleanASCommand;
	import tools.project.controller.ExportCommand;
	import tools.project.controller.ImportProjectCommand;
	import tools.project.controller.Jsfl2SwfCommand;
	import tools.project.controller.LoadProjectCommand;
	import tools.project.controller.NewProjectCommand;
	import tools.project.controller.OpenTextureConfCommand;
	import tools.project.controller.OpenTextureImgCommand;
	import tools.project.controller.PublishProjectCommand;
	import tools.project.controller.SaveProjectCommand;
	import tools.project.controller.TransCommand;
	import tools.texture.controller.DeleteFrameCommand;
	import tools.texture.controller.NewFrameCommand;
	import tools.texture.controller.RebuildTextureCommand;
	import tools.texture.controller.TuneTextureCommand;
	import tools.texture.controller.UpdateFrameCommand;

	/**
	 * @author zhangming.luo
	 */
	public class ConfigControllerCommand extends Command {
		override public function execute() : void {
			commandMap.mapEvent(ActionEditorEvent.SHOW_MAIN, ShowMainCommand);

			commandMap.mapEvent(ActionEditorEvent.ADD_ACTION, AddActionCommand, ActionEditorEvent);
			commandMap.mapEvent(ActionEditorEvent.REMOVE_ACTION, RemoveActionCommand, ActionEditorEvent);
			commandMap.mapEvent(ActionEditorEvent.ADD_FRAME_TO_ACTION, AddFrameToActionCommand, ActionEditorEvent);
			commandMap.mapEvent(ActionEditorEvent.REMOVE_FRAME_FROM_ACTION, RemoveFrameFromActionCommand, ActionEditorEvent);
			commandMap.mapEvent(ActionEditorEvent.CHANGE_FRAME_POSITION, ChangeFramePositionCommand, ActionEditorEvent);

			commandMap.mapEvent(ProjectEvent.BROWSE_TEXTURE_IMG, BrowseTextureImgCommand);
			commandMap.mapEvent(ProjectEvent.BROWSE_TEXTURE_CONFIG, BrowseTextureConfCommand);
			commandMap.mapEvent(ProjectEvent.OPEN_TEXTURE_IMG, OpenTextureImgCommand);
			commandMap.mapEvent(ProjectEvent.OPEN_TEXTURE_CONFIG, OpenTextureConfCommand);

			commandMap.mapEvent(ProjectEvent.PUBLISH, PublishProjectCommand);
			commandMap.mapEvent(ProjectEvent.PUBLISH_FOR_MOBILE, PublishProjectForMobileCommand);
			commandMap.mapEvent(ProjectEvent.SAVE, SaveProjectCommand);
			commandMap.mapEvent(ProjectEvent.LOAD, LoadProjectCommand);
			commandMap.mapEvent(ProjectEvent.NEW, NewProjectCommand);
			commandMap.mapEvent(ProjectEvent.IMPORT, ImportProjectCommand);
			commandMap.mapEvent(ProjectEvent.BATCH_PUBLISH, BatchPublishCommand);
			commandMap.mapEvent(ProjectEvent.TRANS, TransCommand);
			commandMap.mapEvent(ProjectEvent.EXPORT, ExportCommand);
			commandMap.mapEvent(ProjectEvent.JSFL2SWF, Jsfl2SwfCommand);
			commandMap.mapEvent(ProjectEvent.ANI2ANI, Ani2AniCommand);
			commandMap.mapEvent(ProjectEvent.ANITEST, AniTestCommand);
			commandMap.mapEvent(ProjectEvent.CLEANAS, CleanASCommand);

			commandMap.mapEvent(TextureEvent.NEW_FRAME, NewFrameCommand);
			commandMap.mapEvent(TextureEvent.UPDATE_FRAME, UpdateFrameCommand);
			commandMap.mapEvent(TextureEvent.DELETE_FRAME, DeleteFrameCommand);
			commandMap.mapEvent(TextureEvent.TUNE, TuneTextureCommand);
			commandMap.mapEvent(TextureEvent.REBUILD, RebuildTextureCommand);

			dispatch(new ApplicationEvent(ApplicationEvent.CONFIG_SERVICE));
		}
	}
}
