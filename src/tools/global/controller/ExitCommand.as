package tools.global.controller {
	import flash.system.System;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author zhangming.luo
	 */
	public class ExitCommand extends Command {
		override public function execute() : void {
			super.execute();
			System.exit(0);
		}
	}
}
