package tools.global.controller {
	import tools.event.ProjectEvent;
	import tools.global.view.CharacterEditorMainView;
	import tools.model.EditorConfig;

	import org.robotlegs.mvcs.Command;

	import flash.desktop.NativeApplication;
	import flash.events.InvokeEvent;
	import flash.filesystem.File;
	import flash.utils.setTimeout;

	/**
	 * @author zhangming.luo
	 */
	public class ShowMainCommand extends Command {
		[Inject]
		public var config : EditorConfig;

		override public function execute() : void {
			config.jdk = "res\\java.exe";
			config.flash = checkFlash([//
			"C:/Program Files/Adobe/Adobe Flash CS5.5" //
			, "C:/Program Files (x86)/Adobe/Adobe Flash CS5.5" //
			, "C:/Program Files (x86)/Adobe/Adobe Flash CS6" //
			, "C:/Program Files/Adobe/Adobe Flash CS6" //
			, "C:/Program Files (x86)/Adobe/Adobe Flash CS5" //
			, "C:/Program Files/Adobe/Adobe Flash CS5" //
			, "C:/Program Files (x86)/Adobe/Adobe Flash CS4" //
			, "C:/Program Files/Adobe/Adobe Flash CS4" //
			, "D:/Program Files (x86)/Adobe/Adobe Flash CS5.5" //
			, "D:/Program Files/Adobe/Adobe Flash CS5.5" //
			, "D:/Program Files (x86)/Adobe/Adobe Flash CS6" //
			, "D:/Program Files/Adobe/Adobe Flash CS6" //
			, "D:/Program Files (x86)/Adobe/Adobe Flash CS4" //
			, "D:/Program Files/Adobe/Adobe Flash CS4" //
			, "D:/Program Files (x86)/Adobe/Adobe Flash CS5" //
			, "D:/Program Files/Adobe/Adobe Flash CS5" //
			, "E:/Program Files (x86)/Adobe/Adobe Flash CS5.5" //
			, "E:/Program Files/Adobe/Adobe Flash CS5.5" //
			, "E:/Program Files (x86)/Adobe/Adobe Flash CS6" //
			, "E:/Program Files/Adobe/Adobe Flash CS6" //
			, "E:/Program Files (x86)/Adobe/Adobe Flash CS4" //
			, "E:/Program Files/Adobe/Adobe Flash CS4" //
			, "E:/Program Files (x86)/Adobe/Adobe Flash CS5" //
			, "E:/Program Files/Adobe/Adobe Flash CS5" //
			, "F:/Program Files (x86)/Adobe/Adobe Flash CS5.5" //
			, "F:/Program Files/Adobe/Adobe Flash CS5.5" //
			, "F:/Program Files (x86)/Adobe/Adobe Flash CS6" //
			, "F:/Program Files/Adobe/Adobe Flash CS6" //
			, "F:/Program Files (x86)/Adobe/Adobe Flash CS4" //
			, "F:/Program Files/Adobe/Adobe Flash CS4"//
			, "F:/Program Files (x86)/Adobe/Adobe Flash CS5" //
			, "F:/Program Files/Adobe/Adobe Flash CS5"//
			, "C:/Program Files/Adobe Flash CS5.5" //
			, "C:/Program Files (x86)/Adobe Flash CS5.5" //
			, "C:/Program Files (x86)/Adobe Flash CS6" //
			, "C:/Program Files/Adobe Flash CS6" //
			, "C:/Program Files (x86)/Adobe Flash CS5" //
			, "C:/Program Files/Adobe Flash CS5" //
			, "C:/Program Files (x86)/Adobe Flash CS4" //
			, "C:/Program Files/Adobe Flash CS4" //
			, "D:/Program Files (x86)/Adobe Flash CS5.5" //
			, "D:/Program Files/Adobe Flash CS5.5" //
			, "D:/Program Files (x86)/Adobe Flash CS6" //
			, "D:/Program Files/Adobe Flash CS6" //
			, "D:/Program Files (x86)/Adobe Flash CS4" //
			, "D:/Program Files/Adobe Flash CS4" //
			, "D:/Program Files (x86)/Adobe Flash CS5" //
			, "D:/Program Files/Adobe Flash CS5" //
			, "E:/Program Files (x86)/Adobe Flash CS5.5" //
			, "E:/Program Files/Adobe Flash CS5.5" //
			, "E:/Program Files (x86)/Adobe Flash CS6" //
			, "E:/Program Files/Adobe Flash CS6" //
			, "E:/Program Files (x86)/Adobe Flash CS4" //
			, "E:/Program Files/Adobe Flash CS4" //
			, "E:/Program Files (x86)/Adobe Flash CS5" //
			, "E:/Program Files/Adobe Flash CS5" //
			, "F:/Program Files (x86)/Adobe Flash CS5.5" //
			, "F:/Program Files/Adobe Flash CS5.5" //
			, "F:/Program Files (x86)/Adobe Flash CS6" //
			, "F:/Program Files/Adobe Flash CS6" //
			, "F:/Program Files (x86)/Adobe Flash CS4" //
			, "F:/Program Files/Adobe Flash CS4"//
			, "F:/Program Files (x86)/Adobe Flash CS5" //
			, "F:/Program Files/Adobe Flash CS5"//
			]);
			contextView.addChild(new CharacterEditorMainView());
			setTimeout(function() : void {
				// 自动打开
				NativeApplication.nativeApplication.addEventListener(InvokeEvent.INVOKE, onInvoke);
			}, 2000);
		}

		private function onInvoke(event : InvokeEvent) : void {
			if (!event.arguments.length) {
				return;
			}
			var e : ProjectEvent = new ProjectEvent(ProjectEvent.LOAD);
			e.file = File.applicationDirectory.resolvePath(event.arguments[0]);
			dispatch(e);
		}

		private function checkFlash(paths : Array) : String {
			for each (var path : String in paths) {
				var file : File = File.applicationDirectory.resolvePath(path);
				if (file.exists) {
					return file.resolvePath("Flash.exe").nativePath;
				}
			}
			return null;
		}
	}
}
