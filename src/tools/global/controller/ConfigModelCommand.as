package tools.global.controller {
	import tools.event.ApplicationEvent;
	import tools.model.DatabaseConfig;
	import tools.model.EditorConfig;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author zhangming.luo
	 */
	public class ConfigModelCommand extends Command {
		override public function execute() : void {
			injector.mapSingleton(EditorConfig);
			injector.mapSingleton(DatabaseConfig);
			dispatch(new ApplicationEvent(ApplicationEvent.CONFIG_VIEW));
		}
	}
}
