package tools.global.controller {
	import tools.model.EditorConfig;
	import tools.utils.FileHelper;

	import org.robotlegs.mvcs.Command;

	import flash.events.Event;
	import flash.filesystem.File;

	/**
	 * @author zhangming.luo
	 */
	public class ConfigJDKCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		private var file : File;

		override public function execute() : void {
			file = new File();
			file.addEventListener(Event.SELECT, function() : void {
				config.jdk = file.nativePath;
			});
			file.browseForOpen("JDK的java.exe", FileHelper.filters("exe"));
		}
	}
}
