package tools.global.controller {
	import org.aswing.JOptionPane;
	import org.robotlegs.mvcs.Command;

	import kiwi.util.file.FileHelper;

	import tools.model.EditorConfig;

	import flash.events.Event;
	import flash.filesystem.File;

	/**
	 * @author zhangming.luo
	 */
	public class ConfigFlashCommand extends Command {
		[Inject]
		public var config : EditorConfig;
		private var file : File;

		override public function execute() : void {
			file = new File();
			file.addEventListener(Event.SELECT, function() : void {
				config.flash = file.nativePath;
				JOptionPane.showMessageDialog("", "选中" + config.flash);
			});
			file.browseForOpen("flash.exe", FileHelper.filters("exe"));
		}
	}
}
