package tools.global.controller {
	import tools.event.ApplicationEvent;

	import org.robotlegs.mvcs.Command;

	/**
	 * @author zhangming.luo
	 */
	public class ConfigServiceCommand extends Command {
		override public function execute() : void {
			dispatch(new ApplicationEvent(ApplicationEvent.CONFIG_MODEL));
		}
	}
}
